part of 'helpers.dart';



///Layout
Size displaySize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double displayHeight(BuildContext context) {
  return displaySize(context).height;
}

double displayWidth(BuildContext context) {
  return displaySize(context).width;
}

///Transition
int milliseconds = 1000;



///total calc
int calcTotal(List<int> eachTotal){
  int total=0;
  for(int index=0;index<eachTotal.length;index++){
    total+=eachTotal[index];
  }
  return total;
}



String getDate(DateTime time) {
  String date = DateFormat("yyyy/MM/dd").format(time);
  return date;
}

String randomString(int length) {
  const _randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const _charsLength = _randomChars.length;

  final rand = new Random();
  final codeUnits = new List.generate(
    length,
        (index) {
      final n = rand.nextInt(_charsLength);
      return _randomChars.codeUnitAt(n);
    },
  );
  return new String.fromCharCodes(codeUnits);
}



List category = ["Food","Health","Sports","Fitness","Music","Entertain","Politics","Tech","Fashion"];


