part of 'helpers.dart';

ThemeData themeData(context) {
  return ThemeData(
    canvasColor: mainColor,
    cardColor: mainColor,
    appBarTheme: AppBarTheme(color: mainColor, elevation: 0),
    primaryColor: mainColor,
    ///ButtonColor
    buttonTheme: ButtonThemeData(
      minWidth: 300,
      height: 45,
      buttonColor: buttonColor,
      textTheme: ButtonTextTheme.primary,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28.0),
      ),
    ),

    inputDecorationTheme: InputDecorationTheme(

        contentPadding: EdgeInsets.all(15),
        filled: true,
        fillColor: fillColor,
      border: InputBorder.none,
    ),

    ///TextTheme
    textTheme: TextTheme(
        button: TextStyle(
          color: buttonTextColor,
          fontSize: 15,
          // fontWeight: FontWeight.bold
        ),
        ///Large title
        headline1: TextStyle(
            fontWeight: FontWeight.bold, color: fontColor, fontSize: 32),
        ///Title 1
        headline2: TextStyle(
            fontWeight: FontWeight.bold, color: fontColor, fontSize: 28),
        ///Title 2
        headline3: TextStyle(
            fontWeight: FontWeight.bold, color: fontColor, fontSize: 24),
        ///Title 3
        headline4: TextStyle(color: fontSubColor, fontWeight: FontWeight.bold,fontSize: 20),
        ///Title 4
        headline5: TextStyle(color: fontSubColor, fontWeight: FontWeight.bold,fontSize: 18),

        ///Text Large
        headline6: TextStyle(color: fontSubColor, fontSize: 17),

        ///Text mid
        subtitle1: TextStyle(color: fontSubColor,fontSize: 16),
        subtitle2: TextStyle(color: fontSubColor, fontWeight: FontWeight.normal,fontSize: 15),

        ///Text small
        bodyText1: TextStyle(color: fontSubColor,fontSize: 14,fontWeight: FontWeight.w400),
        bodyText2: TextStyle(color: fontSubColor,fontSize: 13),

        ///subhead
        caption: TextStyle(color: subHeadColor,fontSize: 14,fontWeight: FontWeight.w400),
    ),
    ///IconTheme
    iconTheme: IconThemeData(
        color:iconColor
    ),
  );
}
