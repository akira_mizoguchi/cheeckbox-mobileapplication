part of 'helpers.dart';

///app Main
Color mainColor = Colors.white;
Color fontColor = Colors.black;
Color fontSubColor = Colors.black;
Color mainCardColor = Colors.white;
Color subHeadColor = Colors.grey;

///ButtonColor
Color buttonColor = HexColor.fromHex("#3AA0FF");
Color buttonTextColor = Colors.black;

///InputColor
Color fillColor  = Colors.grey.withOpacity(0.1);

///cardColor
Color cardColor = HexColor.fromHex("#2C2E41");

///caption
Color captionColor = Colors.white;

///IconColor
Color iconColor = Colors.black;

///divider
Color dividerColor = Colors.grey;





extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}