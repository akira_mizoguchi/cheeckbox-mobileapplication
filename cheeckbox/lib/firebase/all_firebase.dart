import 'dart:convert';

import 'package:cheeckbox/helpers/helpers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get_connect/http/src/http/utils/body_decoder.dart';
import 'package:get/get_connect/http/src/response/response.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cheeckbox/models/models.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

part 'authentication.dart';
part 'commonuse.dart';
part 'firestore.dart';
part 'firestorage.dart';
part 'Query.dart';