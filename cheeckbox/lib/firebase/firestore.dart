part of "all_firebase.dart";

/// *
/// Add user (use this with authentication sign Up)
/// Related to User
Future<dynamic> createUser(
    String username, DateTime birthday, String gender, String uid,String country) async {
  var users = FirebaseFirestore.instance.collection('Users').doc(uid);
  return users.set({
    'uid': uid,
    'name': username,
    'username': randomString(10),
    'gender': gender,
    'birthday': birthday,
    'profileURL': "",
    'biography': "",
    'country': country,
  }).then((value) {
    print("User Added");
  }).catchError((error) {
    print("Failed to add user: $error");
    return error;
  });
}




/// Delete,Create,Read Question
Future<dynamic> createQuestion(String question, List<String> tags,
    String creator_uid, List<String> options, String URL,String country) {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  var surveys = FirebaseFirestore.instance.collection('Surveys');
  var user = FirebaseFirestore.instance
      .collection("Users")
      .doc(creator_uid)
      .collection("CreatedSurveys");
  var creator = FirebaseFirestore.instance.collection("Users").doc(creator_uid);
  // Call the user's CollectionReference to add a new user
  return surveys.add({
    'creator_uid': creator_uid,
    'creator_uid_ref': creator,
    'question_text': question,
    'choices': options,
    'created_time': DateTime.now(),
    'tags': tags,
    'likes': 0,
    'URL': URL != null ? URL : "",
    'country': country,
  }).then((question_ref) {
    surveys.doc(question_ref.id).collection("Comments");
    surveys.doc(question_ref.id).update({"survey_id": question_ref.id});
  }).catchError((error) {
    print("Failed to add user: $error");
    return error;
  });
}

///Answering Question
Future<void> answeringQuestion(
    String questionId, int selectedIndex, String uid, String choice) async {
  var surveys = FirebaseFirestore.instance
      .collection('Surveys')
      .doc(questionId)
      .collection("Answers");
  var user = FirebaseFirestore.instance
      .collection('Users')
      .doc(uid)
      .collection("AnsweredSurveys");
  var userRef =  FirebaseFirestore.instance.collection('Users').doc(uid);
  var data = await FirebaseFirestore.instance.collection("Users").doc(uid).get().then((value) => value.data());
  await surveys.doc(uid).set({
    "answer_uid": uid,
    "answer_uid_ref": userRef,
    "selected_index": selectedIndex,
    "user_choice": choice,
    "gender": data!["gender"] as String,
    "birthday": data["birthday"] as Timestamp,
  }).then((value) {
    user.doc(questionId).set({"answered_time": DateTime.now()});
  }).catchError((onError) => print(onError));
}

///Get Comments in a quesiton
Future getComments(String questionId) {
  var comments = FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments");
  return comments.get();
}

///comment
Future comment(String questionId, String commenterId, String comment) async {
  print(comment);
  final ref = FirebaseFirestore.instance.collection("Users").doc(commenterId);
  var com = FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .add({
    "comment_text": comment,
    "commenter_uid": commenterId,
    "commenter_uid_ref": ref,
    "comment_time": DateTime.now(),
  }).then((value) {
    value.update({"comment_id": value.id});
  });
}

///get Question creator_uid
Future getQuestionCreatorId(String questionId) async {
  var question =
      FirebaseFirestore.instance.collection("Surveys").doc(questionId);
  var creator_uid = await question.get().then((snapchat) {
    return snapchat.data()!["creator_uid"];
  });
  var user = FirebaseFirestore.instance.collection("Users").doc(creator_uid);

  return user.get();
}

Future followUser(String followUid) async {
  await FirebaseFirestore.instance
      .collection("Users")
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection("Follow")
      .doc(followUid)
      .set({"follow_uid": followUid, "followed_time": DateTime.now()});
  await FirebaseFirestore.instance
      .collection("Users")
      .doc(followUid)
      .collection("Follower")
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .set({
    "follower_uid": FirebaseAuth.instance.currentUser!.uid,
    "followed_time": DateTime.now()
  });
}

Future getFollowOrUnFollow(String followUid) async {
  var answer = FirebaseFirestore.instance
      .collection("Users")
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection("Follow")
      .doc(followUid)
      .get();
  return answer;
}

Future UnFollowUser(String followUid) async {
  await FirebaseFirestore.instance
      .collection("Users")
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection("Follow")
      .doc(followUid)
      .delete();
  return await FirebaseFirestore.instance
      .collection("Users")
      .doc(followUid)
      .collection("Follower")
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .delete();
}

Future updateUserInfo(String uid, String name, String username, String biography) {
  return FirebaseFirestore.instance
      .collection("Users")
      .doc(uid)
      .update({"username": username, "name": name, "biography": biography});
}

/// Like and Unlike comment
Future likeComment(String currentUid, commentId, questionId) async {
  var ref = FirebaseFirestore.instance.collection("Users").doc(currentUid);
  FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Likes")
      .doc(currentUid)
      .set({
    "like_uid": currentUid,
    'like_uid_ref': ref,
    "like_time": DateTime.now()});
}

Future unLikeComment(String currentUid, commentId, questionId) {
  return FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Likes")
      .doc(currentUid)
      .delete();
}

Future getCommentCommenterId(String commenterId) {
  return FirebaseFirestore.instance.collection("Users").doc(commenterId).get();
}

///Replys and add Comments to other Comments /Like Replys and UnlikeReplys
Future getCommentDetailReplys(String commentId, String questionId) {
  return FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Replies")
      .get();
}

Future replyToOtherComments(
    String commentId, String questionId, String replyText) async {
  var comment = await FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Replies")
      .add({
    "reply_text": replyText,
    "reply_time": DateTime.now(),
    "replier_uid": FirebaseAuth.instance.currentUser!.uid,
  }).then((snapshot) {
    snapshot.update({"reply_id": snapshot.id});
  }).catchError((e) {
    debugPrint(e);
  });
  return comment;
}

Future deleteOwnReplyComment(
    String questionId, String commentId, String replyId
) async {
  return FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Replies")
      .doc(replyId
)
      .delete();
}

Future likeReply(String currentUid, commentId, replyId
, questionId) async {
  var ref = FirebaseFirestore.instance.collection("Users").doc(currentUid);
  FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Replies")
      .doc(replyId
)
      .collection("Reply_Likes")
      .doc(currentUid)
      .set({
    "like_uid": currentUid,
    "like_uid_ref": ref,
    "like_time": DateTime.now()
      });
}

Future unLikeReply(String currentUid, replyId, commentId, questionId) {
  return FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Comments")
      .doc(commentId)
      .collection("Replies")
      .doc(replyId)
      .collection("Likes")
      .doc(currentUid)
      .delete();
}

///Delete or edit own question

Future deleteOwnQuestion(String questionId) {
  // FirebaseFirestore.instance.collection("Surveys").doc(questionId).collection("answers").
  return FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .delete();
}

Future editOwnQuestion(String quesitonText, String questionId,
    List<String> options, List<String> tags, String URL) {
  return FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .update({
    'question_text': quesitonText,
    'choices': options,
    'created_time': DateTime.now(),
    'tags': tags,
    'URL': URL != null ? URL : ""
  });
}

///Delete Person
Future<bool> deleteAccount(String uid) async {
  List<String> list =[];
  //delete all document
  var deleteSurveys = await FirebaseFirestore.instance.collection("Surveys").where(
      "creator_uid", isEqualTo: uid).get().then((snap) {
    snap.docs.forEach((element) {
      list.add(element.id);
    });
  });
  for (var docId in list) {
    await FirebaseFirestore.instance.collection("Surveys").doc(docId).delete();
  }

  print("done delete surveys");
  //DeleteFirestore
  var deleteInfo = await FirebaseFirestore.instance.collection("Users").doc(uid).delete().then((value) => true).catchError((onError)=> false);
  //delete account
  var result = await FirebaseAuth.instance.currentUser!.delete().then((value) => true).catchError((error){
    print(error);
    return false;});

  return result;
}
