part of "all_firebase.dart";
/// *
/// Resister with Email
Future<bool> ResisterWithEmail(String email, String password) async {
  try {
    return true;
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      print('The password provided is too weak.');
      return false;
    } else if (e.code == 'email-already-in-use') {
      print('The account already exists for that email.');
      return false;
    }
  } catch (e) {
    print(e);
    return false;
  }
  return false;
}


///Google
Future<UserCredential> signInWithGoogle() async {
  // Trigger the authentication flow
  final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication googleAuth = await googleUser!.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  // Once signed in, return the UserCredential
  return await FirebaseAuth.instance.signInWithCredential(credential);
}

///Check Whether the user is exist or not
Future<bool> checkUserExist(String uid) async {
  bool exist  = await FirebaseFirestore.instance.collection("Users").doc(uid).get().then((value){
    return value.exists;
  });
  return exist;
}