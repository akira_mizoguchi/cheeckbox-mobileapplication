part of 'all_firebase.dart';

Future<dynamic> getPost() {
  return FirebaseFirestore.instance.collection('Surveys').get();
}

Future<dynamic> getSurveyById(String questionId) {
  var question =
      FirebaseFirestore.instance.collection('Surveys').doc(questionId).get();
  return question;
}

Future<dynamic> getUserbyUid(String uid){
  return FirebaseFirestore.instance.collection("Users").doc(uid).get();
}


Future<dynamic> getUser(){
  return FirebaseFirestore.instance.collection("Users").get();
}

Future<dynamic> getPostByText(String text){
  return FirebaseFirestore.instance.collection("Surveys").where("question_text", isGreaterThanOrEqualTo: text).get();
}

Future<dynamic> getUserByUsername(String username){
  return FirebaseFirestore.instance.collection("Users").where("username",isEqualTo: username).get();
}

Future<dynamic> getUserAnswered(String questionId) {
  var answer = FirebaseFirestore.instance
      .collection("Surveys")
      .doc(questionId)
      .collection("Answers")
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .get();
  return answer;
}



///get Whether user answered before
Future<bool> checkAnswered(String docID) async {
  bool answer = false;
  try {
    await FirebaseFirestore.instance
        .collection("Users")
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection("AnsweredSurveys")
        .doc(docID)
        .get()
        .then((doc) {
      if (doc.exists) {
        answer = true;
      } else {
        answer = false;
      }
    });
    return answer;
  } catch (e) {
    print(e);
    return false;
  }
}


///get each Total of answers (All)
Future<List<int>> getSurveyAnswerList(String questionId) async {
  var getList = FirebaseFirestore.instance
      .collection('Surveys')
      .doc(questionId)
      .collection("Answers");
  var question =
      FirebaseFirestore.instance.collection('Surveys').doc(questionId);

  List<int> eachTotal = [];

  var list = await question.get().then((snapshot) async {
    List choices = snapshot.data()!["choices"];
    int loop = 0;
    for (int count = 0; count < choices.length; count++) {
      await getList
          .where('selected_index', isEqualTo: count)
          .get()
          .then((snapshot) {
        eachTotal.add(snapshot.size);


      });
    }
  }).then((value) {
    return eachTotal;
  });
  return list;
}

// Future<List<int>> getSurveyAnswerListFilterd(String questionId) async{
//   var
//
// }
//

///get Question
Future<QuerySnapshot> getSurveysNoOwn(String uid)async {
  return FirebaseFirestore.instance.collection("Surveys").where("creator_uid",isNotEqualTo: uid ).get();

}

Future<QuerySnapshot> getSurveysOwn(String uid)async {
  return FirebaseFirestore.instance.collection("Surveys").where("creator_uid",isEqualTo: uid ).get();

}

Future<QuerySnapshot> getAnsweredSurvey(String uid) async{
  List<String> list=[""];
  var question;
  await FirebaseFirestore.instance.collection("Users").doc(uid).collection("AnsweredSurveys").get().then((snapshot){
    snapshot.docs.forEach((element) {
      list.add(element.id);
    });
  }).catchError((e)=>print(e));

  question  = await FirebaseFirestore.instance.collection("Surveys").where("survey_id",whereIn: list).get();
  return question;
}

Future<QuerySnapshot> getMostVotedQuestion(){
 return FirebaseFirestore.instance.collection("Surveys").orderBy("total",descending: true).get();
}

Future<QuerySnapshot> getSurveysByTag(String tag){
  return FirebaseFirestore.instance.collection("Surveys").where("tags",arrayContains: tag).get();
}




Future<QuerySnapshot> getFollowUser(String uid) async{
  List follows = [""];
  var user = await FirebaseFirestore.instance.collection("Users").doc(uid).collection("Follow").get().then((snapshot){
    snapshot.docs.forEach((element) { 
      follows.add(element.id);
    });
  });

  print(follows);
  var users = await FirebaseFirestore.instance.collection("Users").where("uid",whereIn: follows).get();

  return users;
}

Future<QuerySnapshot> getFollowerUser(String uid) async{
  List follows = [""];
  var user = await FirebaseFirestore.instance.collection("Users").doc(uid).collection("Follower").get().then((snapshot){
    snapshot.docs.forEach((element) {
      follows.add(element.id);
    });
  });

  print(follows);
  var users = await FirebaseFirestore.instance.collection("Users").where("uid",whereIn: follows).get();

  return users;
}


Future<int> getNumberOfFollow(String uid) async {
  var num = await FirebaseFirestore.instance.collection("Users").doc(uid).collection("Follow").get().then((value) => value.size);
  return num;
}

Future<int> getNumberOfFollower(String uid) async {
  var num = await FirebaseFirestore.instance.collection("Users").doc(uid).collection("Follower").get().then((value) => value.size);
  return num;
}


List<Question> parsePhotos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Question>((json) => Question.fromJsonFireStore(json)).toList();
}
