part of "all_firebase.dart";


/***
 * Download from URL
 */
//nw

/**
 * Delete
 */

Future<bool> deleteProfilePic(String uid) async {
  var  firebaseStorageRef = firebase_storage.FirebaseStorage.instance.ref('Users/$uid/profile.png');
  var result = await firebaseStorageRef.delete().then((value) => true).catchError((e)=>false);
  return result;
}