part of "all_firebase.dart";

class FutureWidget extends StatefulWidget {
  const FutureWidget({Key? key}) : super(key: key);

  @override
  _FutureWidgetState createState() => _FutureWidgetState();
}

class _FutureWidgetState extends State<FutureWidget> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        // future: new Future<bool>/Future.wait([list of future]),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text("Something went wrong");
          }
          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data = snapshot.data as Map<String,dynamic>;
            return ListTile(
              //the way to access is data["data title"]
              title: Text(data["username"]),
            );
          }
          return Container();
        });
  }
}