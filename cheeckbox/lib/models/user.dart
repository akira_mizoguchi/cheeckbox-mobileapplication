part of 'models.dart';
class AppUser {
  final String username;
  final String name;
  final Timestamp birthday;
  final String uid;
  final String gender;
  final String bio;
  final String profileURL;


  AppUser(this.username, this.birthday, this.uid, this.gender, this.profileURL, this.bio, this.name);

  AppUser.fromJson(Map<String, dynamic> json):
        name = json["name"],
        username = json["username"],
        birthday =  json["birthday"],
        uid =  json["uid"],
        gender =  json["gender"],
        bio =json["biography"] == null ? "no bio info": json["biography"],
        profileURL =  json["profileURL"];


  // Map<String, dynamic> toJson() => {
  //   'name': name,
  //   'email': email,
  // };

}


