part of 'models.dart';
class Reply {
  final String replyId;
  final String replyText;
  final Timestamp replyTime;
  final String replyerId;
  final int likes;
  // final int dislikes;

  Reply(this.replyId, this.replyText, this.replyTime, this.likes, this.replyerId);

  Reply.fromJson(Map<String, dynamic> json):
        replyText = json["reply_text"],
        replyerId = json["replier_uid"],
        replyTime = json["reply_time"],
        // dislikes = json["dislike"],
        replyId = json["reply_id"],
        likes  =  json["like"] != null ? json["like"] : 0;
}

