part of 'models.dart';
class Question{
  final String creatorId;
  final String questionText;
  final Timestamp postTime;
  final String questionId;
  final List<dynamic> choices;
  final String URL;
  final int like;
  final int total;
  final List<dynamic> tags;
  Question(this.questionText, this.postTime, this.creatorId, this.questionId, this.choices, this.URL, this.like, this.total, this.tags);

  Question.fromJsonFireStore(Map<String, dynamic> json):
        questionId = json["survey_id"],
        creatorId = json["creator_uid"],
        questionText = json["question_text"],
        choices = json["choices"],
        like  =  json["likes"],
        URL = json["URL"],
        tags = json["tags"],
        total = json["total"] == null ? 0 : json["total"],
        postTime = json["created_time"];


  Question.fromJsonElasticSearch(Map<String, dynamic> json):
        questionId = json["questionId"],
        creatorId = json["creatorId"],
        questionText = json["question"],
        choices = json["choice"],
        like  =  json["like"],
        URL = json["URL"],
        tags = json["tags"],
        total = json["total"],
        postTime = Timestamp(json["createdTime"]["_seconds"], json["createdTime"]["_nanoseconds"]);


  Map<String, dynamic> toJson() => {
    'questionText': questionText,
    'questionId': postTime,

  };
}