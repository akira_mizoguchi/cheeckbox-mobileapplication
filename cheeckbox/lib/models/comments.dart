part of 'models.dart';
class Comment {
  final String commenterId;
  final String commentText;
  final Timestamp commentTime;
  final String commentId;
  final int likes;
  Comment(this.commentText, this.likes, this.commenterId, this.commentTime, this.commentId);

  Comment.fromJson(Map<String, dynamic> json):
        commentText = json["comment_text"] != null ? json["comment_text"] : "",
        commenterId = json["commenter_uid"] != null ? json["commenter_uid"] : "",
        commentTime = json["comment_time"] != null ? json["comment_time"] : null,
        commentId = json["comment_id"] != null ? json["comment_id"]:null,
        likes  =  json["like"] != null ? json["like"]: 0;
}

