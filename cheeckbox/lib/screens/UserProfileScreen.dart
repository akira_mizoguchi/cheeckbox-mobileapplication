part of 'Screens.dart';

class UserProfileScreen extends StatelessWidget {
  final String uid;

  const UserProfileScreen({Key? key, required this.uid}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        getUserbyUid(uid),
        getFollowOrUnFollow(uid),
        getNumberOfFollow(uid),
        getNumberOfFollower(uid)
      ]),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          AppUser user = AppUser.fromJson(snapshot.data[0].data());
          print(snapshot.data[2]);

          return UserProfileScreenBody(
            user: user,
            follow: snapshot.data[1].data(),
            numberOfFollow: snapshot.data[2],
            numberOfFollower: snapshot.data[3],
          );
        } else {
          return Text("");
        }
      },
    );
  }
}

class UserProfileScreenBody extends StatefulWidget {
  final AppUser user;
  final follow;
  final int numberOfFollow;
  final int numberOfFollower;

  const UserProfileScreenBody({
    Key? key,
    required this.user,
    this.follow,
    required this.numberOfFollow,
    required this.numberOfFollower,
  }) : super(key: key);

  @override
  _UserProfileScreenBodyState createState() => _UserProfileScreenBodyState();
}

class _UserProfileScreenBodyState extends State<UserProfileScreenBody> {
  late Notification notification;
  late ScrollController _scrollController;
  bool onTop = false;
  bool followed = false;

  final List<Tab> myTabs = <Tab>[
    Tab(
      text: 'Survey',
    ),
    Tab(text: 'Answered'),
  ];

  @override
  void initState() {
    // TODO: implement initState
    if (widget.follow == null) {
      followed = false;
    } else {
      followed = true;
    }
    _scrollController = ScrollController();
    super.initState();
  }

  Future? _navigationToUserList() {
    return Get.to(
      UserListScreen(
        uid: widget.user.uid,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: onTop
                ? AppBar(
                    flexibleSpace: (MyFadeTransition(
                        child: UserFlexibleSpace(
                    user: widget.user,
                  ))))
                : AppBar(
                    title: MyFadeTransition(
                    child: Text("@${widget.user.username}"),
                  )),
            body: NotificationListener(
                // onNotification: (notification) {
                //   setState(() {
                //     if (_scrollController.position.pixels > 20) {
                //       onTop = true;
                //     } else {
                //       onTop = false;
                //     }
                //   });
                // },
                child: NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder: (BuildContext context, bool isScrolled) {
                return [
                  SliverToBoxAdapter(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 30),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          // padding: EdgeInsets.all(4),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color:
                                                  Colors.blue.withOpacity(0.4)),
                                          child: CachedNetworkImage(
                                            imageUrl: widget.user.profileURL,
                                            imageBuilder:
                                                (context, imageProvider) =>
                                                    CircleAvatar(
                                              backgroundImage: imageProvider,
                                              radius: 30,
                                            ),
                                            placeholder: (context, url) =>
                                                CircleAvatar(
                                              radius: 30,
                                            ),
                                            errorWidget: (context, url,
                                                    error) =>
                                                CircleAvatar(
                                                    backgroundImage: AssetImage(
                                                        'assets/user.png'),
                                                    radius: 30),
                                          )),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        widget.user.username,
                                        style: theme.textTheme.subtitle1,
                                      ),
                                      Text(
                                        "@${widget.user.username}",
                                        style: theme.textTheme.caption,
                                      )
                                    ]),
                                Expanded(
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              onTap: _navigationToUserList,
                                              child: ClipOval(
                                                  child: Container(
                                                      padding:
                                                          EdgeInsets.all(15),
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                              widget
                                                                  .numberOfFollower
                                                                  .toString(),
                                                              style: theme
                                                                  .textTheme
                                                                  .headline5),
                                                          Text(' Follower',
                                                              style: theme
                                                                  .textTheme
                                                                  .caption),
                                                        ],
                                                      )))),
                                          InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              onTap: _navigationToUserList,
                                              child: ClipOval(
                                                  child: Container(
                                                padding: EdgeInsets.all(15),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                        widget.numberOfFollow
                                                            .toString(),
                                                        style: theme.textTheme
                                                            .headline5),
                                                    Text(' Follow',
                                                        style: theme
                                                            .textTheme.caption),
                                                  ],
                                                ),
                                              )))
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ]),
                        ),

                        ///Follow / UnFollow

                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left: 15, right: 15),
                          child: Text(
                            widget.user.bio.toString(),
                            style: theme.textTheme.bodyText1,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                            alignment: Alignment.center,
                            child: OutlineButton(
                              onPressed: () {
                                if (followed == false) {
                                  followUser(widget.user.uid).then((value) {
                                    setState(() {
                                      followed = true;
                                    });
                                  }).catchError((onError) => print(onError));
                                } else {
                                  UnFollowUser(widget.user.uid).then((value) {
                                    setState(() {
                                      followed = false;
                                    });
                                  }).catchError((e) => print(e));
                                }
                              },
                              child: Text(
                                followed ? "Unfollow" : "Follow +",
                                style: followed
                                    ? theme.textTheme.headline6!
                                        .copyWith(color: Colors.grey)
                                    : theme.textTheme.headline6,
                              ),
                            )),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  SliverPersistentHeader(
                    pinned: true,
                    delegate: SliverPersistentHeaderA(
                        tabBar: TabBar(
                      unselectedLabelColor: Colors.grey,
                      tabs: myTabs,
                    )),
                  )
                ];
              },
              body: TabBarView(
                children: [
                  FuturePostListView(
                    future: getSurveysOwn(widget.user.uid),
                  ),
                  FuturePostListView(
                      future: getAnsweredSurvey(widget.user.uid))
                ],
              ),
            ))));
  }
}

class SliverPersistentHeaderA extends SliverPersistentHeaderDelegate {
  final TabBar tabBar;
  final Color color;

  const SliverPersistentHeaderA({
    Color color = Colors.transparent,
    required this.tabBar,
  }) : this.color = color;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final theme = Theme.of(context);
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(5),
      color: theme.primaryColor,
      child: tabBar,
    );
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => tabBar.preferredSize.height;

  @override
  // TODO: implement minExtent
  double get minExtent => tabBar.preferredSize.height;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return true;
  }
}
