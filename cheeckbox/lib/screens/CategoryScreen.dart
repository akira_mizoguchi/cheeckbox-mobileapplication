part of 'Screens.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return CustomScrollView(
      physics: NeverScrollableScrollPhysics(),
      primary: true,
      slivers: <Widget>[
        ///Category
        SliverToBoxAdapter(
          child: Container(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              "Category",
              style: theme.textTheme.headline3,
            ),
          ),
        ),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return Card(
              elevation: 10,
              color: theme.primaryColor,
              child: ListTile(
                title: Text(
                  "${category[index]}",
                  style: theme.textTheme.headline6,
                ),
              ),
            );
          }, childCount: category.length),
        ),
        SliverToBoxAdapter(
          child: Container(
            padding: EdgeInsets.only(left: 10, top: 10),
            child: Text(
              "Trend #",
              style: theme.textTheme.headline3,
            ),
          ),
        ),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return Card(
              elevation: 10,
              color: theme.primaryColor,
              child: ListTile(
                leading: Icon(
                  Icons.tag,
                  color: Colors.white,
                ),
                title: Text("Food"),
              ),
            );
          }, childCount: 7),
        ),

        ///Trend Tag

        ///Recent
      ],
    );
  }
}
