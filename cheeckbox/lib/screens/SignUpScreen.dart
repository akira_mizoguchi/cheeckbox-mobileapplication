part of "Screens.dart";

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  @override
  void dispose() {
    emailTextController.dispose();
    passwordTextController.dispose();
    super.dispose();
  }

  Future? _navigationToUserForm() {
    return Get.to(
      UserInfoFormScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    double height = displayHeight(context);
    double width = displayWidth(context);
    return Scaffold(
        appBar: AppBar(),
        body: Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Sign-Up",
                  style: theme.textTheme.headline1,
                ),
                SizedBox(
                  height: 15,
                ),
                // TextField(
                //   controller: emailTextController,
                //   decoration: InputDecoration(
                //       enabledBorder: const OutlineInputBorder(
                //         borderRadius: BorderRadius.all(Radius.circular(20)),
                //         borderSide: const BorderSide(
                //           color: Colors.grey,
                //           width: 1.2,
                //         ),
                //       ),
                //       focusedBorder: const OutlineInputBorder(
                //         borderRadius: BorderRadius.all(Radius.circular(20)),
                //         borderSide:
                //             const BorderSide(color: Colors.grey, width: 1.2),
                //       ),
                //       filled: true,
                //       fillColor: theme.primaryColor,
                //       hintText: "E-mail address",
                //       hintStyle: theme.textTheme.caption),
                // ),
                // SizedBox(
                //   height: 15,
                // ),
                // TextField(
                //   controller: passwordTextController,
                //   decoration: InputDecoration(
                //       enabledBorder: const OutlineInputBorder(
                //         borderRadius: BorderRadius.all(Radius.circular(20)),
                //         borderSide: const BorderSide(
                //           color: Colors.grey,
                //           width: 1.2,
                //         ),
                //       ),
                //       focusedBorder: const OutlineInputBorder(
                //         borderRadius: BorderRadius.all(Radius.circular(20)),
                //         borderSide:
                //             const BorderSide(color: Colors.grey, width: 1.2),
                //       ),
                //       filled: true,
                //       fillColor: theme.primaryColor,
                //       hintText: "Password",
                //       hintStyle: theme.textTheme.caption),
                // ),
                // Spacer(
                //   flex: 1,
                // ),
                // Center(
                //     child: RaisedButton(
                //   child: Text("Sign-Up"),
                //   onPressed: () {
                //     ResisterWithEmail(emailTextController.text,
                //             passwordTextController.text)
                //         .then((value) {
                //       print(value);
                //       if (value == true) {
                //         _navigationToUserForm();
                //       }
                //     });
                //   },
                // )),
                Center(
                    child: SignInButton(Buttons.Apple,
                        text: "Sign up with Apple", onPressed: () {
                  var user = signInWithGoogle().then((value) {
                    if (value != null) {
                      print(value.user);
                      _navigationToUserForm();
                    }
                  });
                })),
                SizedBox(
                  height: 10,
                ),
                Center(
                    child: Text(
                  "Or",
                  style: theme.textTheme.caption,
                )),
                SizedBox(
                  height: 10,
                ),
                Center(
                    child: SignInButton(Buttons.Google,
                        text: "Sign up with Google", onPressed: () {
                  var user = signInWithGoogle().then((value) {
                    if (value != null) {
                      print(value.user);
                      _navigationToUserForm();
                    }
                  });
                })),
                SizedBox(
                  height: 10,
                ),
                // Center(
                //     child: SignInButton(
                //   Buttons.Facebook,
                //   text: "Sign up with Google",
                //   onPressed: () {
                //     signInWithGoogle();
                //   },
                // )),
                Spacer(
                  flex: 20,
                )
              ],
            )));
  }
}
