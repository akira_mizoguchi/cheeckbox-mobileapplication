part of 'Screens.dart';

class OwnerScreen extends StatelessWidget {
  const OwnerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String uid = FirebaseAuth.instance.currentUser!.uid;
    return FutureBuilder(
      future: Future.wait([
        getUserbyUid(uid),
        getNumberOfFollow(uid),
        getNumberOfFollower(uid)
      ]),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          AppUser user = AppUser.fromJson(snapshot.data[0].data());

          return OwnerScreenBody(
            user: user,
            numberOfFollow: snapshot.data[1],
            numberOfFollower: snapshot.data[2],
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class OwnerScreenBody extends StatefulWidget {
  final AppUser user;
  final int numberOfFollow;
  final int numberOfFollower;

  const OwnerScreenBody(
      {Key? key,
      required this.user,
      required this.numberOfFollow,
      required this.numberOfFollower})
      : super(key: key);

  @override
  _OwnerScreenBodyState createState() => _OwnerScreenBodyState();
}

class _OwnerScreenBodyState extends State<OwnerScreenBody> {
  late ScrollController _scrollController;
  bool onTop = false;

  final List<Tab> myTabs = <Tab>[
    // Tab(text: 'Pined'),
    Tab(
      text: 'Your Survey',
    ),
    Tab(text: 'Answered'),
  ];

  @override
  void initState() {
    // TODO: implement initState
    _scrollController = ScrollController();
    super.initState();
  }

  Future? _navigationToUserList() {
    return Get.to(
      UserListScreen(
        uid: widget.user.uid,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  Future? _navigationToEdit() {
    return Get.to(
      EditScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return DefaultTabController(
        length: myTabs.length,
        child: Scaffold(
            appBar: onTop
                ? AppBar(
                    automaticallyImplyLeading: false,
                    flexibleSpace: (MyFadeTransition(
                        child: OwnerFlexibleSpace(
                      user: widget.user,
                    ))))
                : AppBar(
                    automaticallyImplyLeading: false,
                    actions: [
                      IconButton(
                        icon: Icon(
                          Icons.settings,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          _navigationToEdit();
                        },
                      )
                    ],
                    title: MyFadeTransition(
                      child: Text("${widget.user.name}"),
                    )),
            body: NotificationListener(
                child: NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder: (BuildContext context, bool isScrolled) {
                return [
                  SliverToBoxAdapter(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 30),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          // padding: EdgeInsets.all(4),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color:
                                                  Colors.blue.withOpacity(0.4)),
                                          child: CachedNetworkImage(
                                            imageUrl: widget.user.profileURL,
                                            imageBuilder:
                                                (context, imageProvider) =>
                                                    CircleAvatar(
                                              backgroundImage: imageProvider,
                                              radius: 30,
                                            ),
                                            placeholder: (context, url) =>
                                                CircleAvatar(
                                              radius: 30,
                                            ),
                                            errorWidget:
                                                (context, url, error) =>
                                                    CircleAvatar(
                                              radius: 30,
                                              backgroundImage:
                                                  AssetImage('assets/user.png'),
                                            ),
                                          )),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        widget.user.name,
                                        style: theme.textTheme.subtitle1,
                                      ),
                                      Text(
                                        "@${widget.user.username}",
                                        style: theme.textTheme.caption,
                                      )
                                    ]),
                                Expanded(
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              onTap: _navigationToUserList,
                                              child: ClipOval(
                                                  child: Container(
                                                      padding:
                                                          EdgeInsets.all(15),
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                              widget
                                                                  .numberOfFollower
                                                                  .toString(),
                                                              style: theme
                                                                  .textTheme
                                                                  .headline5),
                                                          Text(' Follower',
                                                              style: theme
                                                                  .textTheme
                                                                  .caption),
                                                        ],
                                                      )))),
                                          InkWell(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              onTap: _navigationToUserList,
                                              child: ClipOval(
                                                  child: Container(
                                                padding: EdgeInsets.all(15),
                                                child: Column(
                                                  children: [
                                                    Text(
                                                        widget.numberOfFollow
                                                            .toString(),
                                                        style: theme.textTheme
                                                            .headline5),
                                                    Text(' Follow',
                                                        style: theme
                                                            .textTheme.caption),
                                                  ],
                                                ),
                                              )))
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ]),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        // Container(alignment: Alignment.centerLeft,child: Text("bio")),
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.only(left: 15, right: 15),
                          child: Text(
                            widget.user.bio.toString(),
                            style: theme.textTheme.bodyText1,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                      ],
                    ),
                  ),
                  SliverPersistentHeader(
                    pinned: true,
                    delegate: SliverPersistentHeaderA(
                        tabBar: TabBar(
                      unselectedLabelColor: Colors.grey,
                      // indicatorWeight: 0.6,
                      automaticIndicatorColorAdjustment: false,
                      tabs: myTabs,
                    )),
                  )
                ];
              },
              body: TabBarView(
                children: [
                  // Text("hei"),
                  FutureOwnPostListView(
                    future:
                        getSurveysOwn(FirebaseAuth.instance.currentUser!.uid),
                  ),
                  FuturePostListView(
                    future: getAnsweredSurvey(
                        FirebaseAuth.instance.currentUser!.uid),
                  )
                ],
              ),
            ))));
  }
}
