part of 'Screens.dart';

class EditQuestionScreen extends StatelessWidget {
  final String questionId;

  const EditQuestionScreen({Key? key, required this.questionId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getSurveyById(questionId),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          Question question = Question.fromJsonFireStore(snapshot.data.data());
          return EditQuestionScreenBody(
            question: question,
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          return Container();
        }
      },
    );
  }
}

class EditQuestionScreenBody extends StatefulWidget {
  final Question question;

  const EditQuestionScreenBody({Key? key, required this.question})
      : super(key: key);

  @override
  _EditQuestionScreenBodyState createState() => _EditQuestionScreenBodyState();
}

class _EditQuestionScreenBodyState extends State<EditQuestionScreenBody> {
  final optionTextController = new TextEditingController();
  final URLTextController = new TextEditingController();
  final TagTextController = new TextEditingController();
  final questionTextController = new TextEditingController();
  final focusNode = FocusNode();
  List<String> listItems = [];
  List<String> tags = [];

  void reorderData(int oldIndex, int newIndex) {
    setState(() {
      if (newIndex > oldIndex) {
        newIndex -= 1;
      }
      final items = listItems.removeAt(oldIndex);
      listItems.insert(newIndex, items);
      print(listItems);
    });
  }

  Future? _navigationOff() {
    return Get.off(
      Home(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  initState() {
    widget.question.choices.forEach((element) {
      listItems.add(element.toString());
    });
    URLTextController.text = widget.question.URL;
    questionTextController.text = widget.question.questionText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    void alartEdit() {}

    return GestureDetector(
        onTap: () {
          focusNode.unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
              onTap: () {
                _navigationOff();
              },
              child: Container(
                  padding: EdgeInsets.only(left: 10),
                  alignment: Alignment.center,
                  child: Text(
                    "Cancel",
                    style: TextStyle(color: Colors.blue),
                  )),
            ),
            actions: [
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.blue),
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  child: InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            backgroundColor: theme.primaryColor,
                            title: new Text("Finish Editing?"),
                            content: new Text(
                                "If you edit your question the result also be changed. is it okay to change?"),
                            actions: <Widget>[
                              new TextButton(
                                child: new Text("Cancel"),
                                onPressed: () {
                                  Get.back();
                                },
                              ),
                              new TextButton(
                                child: new Text("OK"),
                                onPressed: () {
                                  editOwnQuestion(
                                          questionTextController.text,
                                          widget.question.questionId,
                                          listItems,
                                          tags,
                                          URLTextController.text)
                                      .then((value) {
                                    _navigationOff();
                                  }).catchError((e) {
                                    _navigationOff();
                                    print(e);
                                  });
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Text(
                      "Done",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  )),
            ],
          ),
          body: Builder(builder: (BuildContext context) {
            return ReorderableListView(
              shrinkWrap: true,
              header: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Question",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        focusNode: focusNode,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Write Question..."),
                        maxLines: 5,
                        controller: questionTextController,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "URL",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Here is the link..."),
                        maxLines: null,
                        controller: URLTextController,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Tag #",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextFieldTags(
                          tagsStyler: TagsStyler(
                              showHashtag: true,
                              tagTextStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                              tagDecoration: BoxDecoration(
                                color: Colors.blue[300],
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              tagCancelIcon: Icon(Icons.cancel,
                                  size: 18.0, color: Colors.blue[900]),
                              tagPadding: const EdgeInsets.all(6.0)),
                          textFieldStyler: TextFieldStyler(
                              textFieldFilled: true,
                              textFieldBorder: InputBorder.none,
                              textFieldFilledColor:
                                  theme.inputDecorationTheme.fillColor,
                              hintText: "Type your tags...",
                              helperText: "less than 15 chars",
                              helperStyle: TextStyle(color: Colors.grey),
                              hintStyle: TextStyle(color: Colors.grey)),
                          onTag: (tag) {
                            tags.add(tag);
                            print(tags);
                          },
                          onDelete: (tag) {
                            tags.remove(tag);
                            print(tags);
                          },
                          validator: (tag) {
                            if (tag!.length > 15) {
                              return "should be less than 15";
                            }
                            return null;
                          })),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Choice",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Write Choise..."),
                        maxLines: null,
                        controller: optionTextController,
                      )),
                ],
              ),
              children: <Widget>[
                for (final items in listItems)
                  Padding(
                      key: Key(items),
                      padding: EdgeInsets.all(5),
                      child: MyFadeTransition(
                          child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                        child: ListTile(
                          title: Padding(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: Text(items)),
                          leading: Text("${listItems.indexOf(items) + 1}"),
                          trailing: IconButton(
                            icon: Icon(
                              Icons.delete,
                              color: Colors.grey,
                            ),
                            onPressed: () {
                              setState(() {
                                listItems.remove(items);
                                print(listItems);
                              });
                            },
                          ),
                        ),
                      ))),
                SizedBox(
                  height: 100,
                  key: Key("space"),
                )
              ],
              onReorder: reorderData,
            );
          }),
          floatingActionButton: GestureDetector(
            child: Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.blue),
              child: Text(
                "Add+",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
            onTap: () => {
              setState(() {
                listItems.add(optionTextController.text);
                optionTextController.text = "";
                print(listItems);
              }),
            },
          ),
        ));
  }
}
