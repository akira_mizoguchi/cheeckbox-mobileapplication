part of "Screens.dart";

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  double opacityLevel = 1.0;

  String generateNonce([int length = 32]) {
    final charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<UserCredential> signInWithAppleId() async {
    // To prevent replay attacks with the credential returned from Apple, we
    // include a nonce in the credential request. When signing in with
    // Firebase, the nonce in the id token returned by Apple, is expected to
    // match the sha256 hash of `rawNonce`.
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    // Request credential for the currently signed in Apple account.
    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      nonce: nonce,
    );

    // Create an `OAuthCredential` from the credential returned by Apple.
    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    // Sign in the user with Firebase. If the nonce we generated earlier does
    // not match the nonce in `appleCredential.identityToken`, sign in will fail.
    return await FirebaseAuth.instance.signInWithCredential(oauthCredential);
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    Future? _navigationToSignIn() {
      return Get.to(
        SignInScreen(),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }

    Future? _navigationToSignUp() {
      return Get.to(
        SignUpScreen(),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }

    Future? _navigationToHome() {
      return Get.to(
        Home(),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }

    Future? _navigationToUserForm() {
      return Get.to(
        UserInfoFormScreen(),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }

    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/welcomePic.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomCenter,
              stops: [0.1, 0.8],
              colors: [
                Colors.white70.withOpacity(0.4),
                Colors.white.withOpacity(1),
              ],
            ),
          ),
          child: Column(children: [
            Spacer(
              flex: 10,
            ),
            Expanded(
                flex: 40,
                child: Container(
                  alignment: Alignment.center,
                  // child: Image.asset("assets/app_icon.png",
                  //     height: 200, width: 200)
                )),
            Spacer(
              flex: 20,
            ),
            Expanded(
              flex: 5,
              child: AnimatedTextKit(
                animatedTexts: [
                  FadeAnimatedText("Create Your Survey",
                      textStyle: theme.textTheme.headline1),
                  FadeAnimatedText("View The Result",
                      textStyle: theme.textTheme.headline1),
                  FadeAnimatedText("Let's Get Started!!",
                      textStyle: theme.textTheme.headline1),
                ],
                isRepeatingAnimation: true,
                repeatForever: true,
              ),
            ),
            Expanded(
              flex: 5,
              child: Text(
                "SignUp From Below",
                style: theme.textTheme.subtitle1!.copyWith(color: Colors.grey),
              ),
            ),
            Center(
                child:
                // Padding(
                    // padding: EdgeInsets.only(left: 20, right: 20),
                    // child:
                RaisedButton(
                  color: Colors.black,
                      onPressed: () async {
                        final user = await signInWithAppleId();
                        print(user);
                        bool exit = await checkUserExist(user.user!.uid);
                        if (exit) {
                          _navigationToHome();
                        } else {
                          _navigationToUserForm();
                        }
                        // Now send the credential (especially `credential.authorizationCode`) to your server to create a session
                        // after they have been validated with Apple (see `Integration` section for more information on how to do this)
                      },
                  child:  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          "assets/apple_logo.png",
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            'Sign in with Apple',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
        )
    ),
            SizedBox(
              height: 10,
            ),
            Center(
                child: Text(
              "Or",
              style: theme.textTheme.caption,
            )),
            SizedBox(
              height: 10,
            ),
            Center(
              child: RaisedButton(
                color: Colors.white,
                onPressed: () async {
                  var data = await signInWithGoogle();
                  bool exit = await checkUserExist(data.user!.uid);
                  if (exit) {
                    _navigationToHome();
                  } else {
                    _navigationToUserForm();
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/google_logo.png",
                        height: 24,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          'Sign in with Google',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.grey,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Spacer(
              flex: 3,
            ),
            Expanded(
                flex: 3,
                child: Text(
                  "do you have account?",
                )),
            Spacer(
              flex: 10,
            )
          ])),
    ));
  }
}
