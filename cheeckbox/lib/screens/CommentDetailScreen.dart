part of 'Screens.dart';

class CommentDetailScreen extends StatefulWidget {
  final Comment comment;
  final String questionId;
  final int totalLikes;

  const CommentDetailScreen(
      {Key? key,
      required this.comment,
      required this.questionId,
      required this.totalLikes})
      : super(key: key);

  @override
  _CommentDetailScreenState createState() => _CommentDetailScreenState();
}

class _CommentDetailScreenState extends State<CommentDetailScreen> {
  final commentTextController = TextEditingController();

  Widget BuildComment() {
    return FutureReplyComments(
      questionId: widget.questionId,
      commentId: widget.comment.commentId,
    );
  }

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        // elevation: 10,
        automaticallyImplyLeading: false,
        flexibleSpace: FutureUserFlexibleSpace(
          future: getCommentCommenterId(widget.comment.commenterId),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
              child: ListTile(
                  tileColor: theme.primaryColor,
                  title: Text(
                    getDate(widget.comment.commentTime.toDate()),
                    style: theme.textTheme.caption,
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///Comments
                      Text(
                        widget.comment.commentText,
                        style: theme.textTheme.subtitle2,
                      ),

                      ///Like dislike Button
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.thumb_up,
                            color: Colors.blue.withOpacity(0.7),
                          ),
                          Text(
                            " ${widget.totalLikes.toString()}",
                            style: theme.textTheme.bodyText2,
                          ),
                          Text(
                            " likes",
                            style: theme.textTheme.caption,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        color: dividerColor,
                      )
                    ],
                  ))),

          ///CommentAuther
          Expanded(child: BuildComment()),
          Container(
            color: theme.primaryColor,
            padding: EdgeInsets.only(bottom: 10, left: 2, right: 2, top: 10),
            child: ListTile(
                title: TextFormField(
                  controller: commentTextController,
                  decoration: InputDecoration(
                    hintText: "Write message...",
                  ),
                ),
                trailing: IconButton(
                  icon: Icon(
                    Icons.send,
                    color: theme.buttonColor,
                  ),
                  onPressed: () async {
                    await replyToOtherComments(widget.comment.commentId,
                            widget.questionId, commentTextController.text)
                        .then((value) {
                      commentTextController.text = "";
                      BuildComment();
                    }).catchError((e) => print(e));
                  },
                )),
          ),
        ],
      ),
    );
  }
}

//
// ListView.builder(
//     padding: EdgeInsets.only(bottom: 100),
//     itemCount: 5,
//     itemBuilder: (context, index) {
//       return ListTile(
//           tileColor: theme.primaryColor,
//           leading: Column(
//             children: [
//               Expanded(child: CircleAvatar()),
//               Text(
//                 "username",
//                 style: theme.textTheme.caption,
//               ),
//             ],
//           ),
//           title: Text("6 month ago",style: theme.textTheme.caption,),
//           subtitle: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               ///Comments
//               Text(
//                   "how people can think this kind of things huh ?\n can you guuys imagine how peeople can deal all these problems ?",style: theme.textTheme.subtitle2,),
//
//               ///Like dislike Button
//               SizedBox(
//                 height: 10,
//               ),
//               Row(
//                 children: [
//                   Icon(Icons.thumb_up),
//                   Text(" 100 "),
//                   Icon(Icons.thumb_down),
//                   Text(" 100 "),
//                 ],
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Divider(color: dividerColor,)
//             ],
//           ));
//     })
