part of 'Screens.dart';

class TopScreen extends StatefulWidget {
  const TopScreen({Key? key}) : super(key: key);

  @override
  _TopScreenState createState() => _TopScreenState();
}

class _TopScreenState extends State<TopScreen> {
  Future? _navigationToNotification() {
    return Get.to(
      NotificationScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final List<Tab> myTabs = <Tab>[
      Tab(
        text: "Survey",
      ),
      Tab(
        text: "Answered",
      ),
    ];

    return DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool isScrolled) {
            return [
              CupertinoSliverNavigationBar(
                backgroundColor: theme.primaryColor,
                automaticallyImplyLeading: false,
                largeTitle: Text(
                  "Home",
                  style: TextStyle(color: theme.textTheme.headline1!.color),
                ),
                // trailing: IconButton(
                //   icon: Icon(Icons.notification_important),
                //   onPressed: _navigationToNotification,
                // ),
              ),
              SliverPersistentHeader(
                  pinned: true,
                  delegate: SliverPersistentHeaderA(
                      tabBar: TabBar(
                    unselectedLabelColor: Colors.grey,
                    tabs: myTabs,
                  ))),
            ];
          },
          body: TabBarView(
            children: [
              FuturePostListView(
                future:
                    getSurveysNoOwn(FirebaseAuth.instance.currentUser!.uid),
              ),
              FuturePostListView(
                future:
                    getAnsweredSurvey(FirebaseAuth.instance.currentUser!.uid),
              ),
            ],
          ),
        ));
  }
}
