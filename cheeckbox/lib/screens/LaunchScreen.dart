part of "Screens.dart";

class LaunchScreen extends StatelessWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  Future<Widget> checkExistFirestore() async {
    final auth = FirebaseAuth.instance.currentUser;
    if (auth != null) {
      final snapShot = await FirebaseFirestore.instance
          .collection('Users')
          .doc(auth.uid)
          .get();
      if (snapShot.exists) {
        // Document already exists
        return Home();
      } else {
        // Document doesn't exist
        print("dont exist");
        return UserInfoFormScreen();
      }
    }
    return WelcomeScreen();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return FutureBuilder(
      future: checkExistFirestore(),
      builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
        if (snapshot.hasData) {
          final widget = snapshot.data as Widget;
          return SplashScreenView(
            text: "CheeckBox",
            textStyle: theme.textTheme.headline1!
                .copyWith(fontWeight: FontWeight.bold),
            imageSrc: "assets/logo.png",
            duration: 3000,
            imageSize: 120,
            backgroundColor: theme.canvasColor,
            navigateRoute: widget,
          );
        }
        return SplashScreenView(
          imageSrc: "assets/logo.png",
          duration: 3000,
          imageSize: 120,
          backgroundColor: theme.canvasColor,
          navigateRoute: WelcomeScreen(),
        );
      },
    );
  }
}
