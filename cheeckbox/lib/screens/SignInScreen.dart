part of "Screens.dart";

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  var passwordTextController;
  var emailTextController;

  Future? _navigationToHome() {
    return Get.to(
      Home(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  Future? _navigationToUserForm() {
    return Get.to(
      UserInfoFormScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    double height = displayHeight(context);
    double width = displayWidth(context);

    return Scaffold(
        appBar: AppBar(),
        body: Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Sign-In",
                  style: theme.textTheme.headline1,
                ),
                SizedBox(
                  height: 15,
                ),
                TextField(
                  controller: emailTextController,
                  decoration: InputDecoration(
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: const BorderSide(
                          color: Colors.grey,
                          width: 1.2,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1.2),
                      ),
                      filled: true,
                      fillColor: theme.primaryColor,
                      hintText: "E-mail address",
                      hintStyle: theme.textTheme.caption),
                ),
                SizedBox(
                  height: 15,
                ),
                TextField(
                  controller: passwordTextController,
                  decoration: InputDecoration(
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: const BorderSide(
                          color: Colors.grey,
                          width: 1.2,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 1.2),
                      ),
                      filled: true,
                      fillColor: theme.primaryColor,
                      hintText: "Password",
                      hintStyle: theme.textTheme.caption),
                ),
                Spacer(
                  flex: 1,
                ),
                Center(
                    child: OutlineButton(
                  borderSide: BorderSide(color: Colors.black),
                  child: Text(
                    "Sign-Up",
                    style: theme.textTheme.headline5,
                  ),
                  onPressed: () {
                    ResisterWithEmail(emailTextController.text,
                            passwordTextController.text)
                        .then((value) {
                      print(value);
                      if (value == true) {
                        _navigationToHome();
                      }
                    });
                  },
                )),

                Center(
                    child: Text(
                  "Or",
                  style: theme.textTheme.caption,
                )),
                Center(
                    child: SignInButton(Buttons.Google,
                        text: "Sign up with Google", onPressed: () async {
                  var data = await signInWithGoogle();
                  bool exit = await checkUserExist(data.user!.uid);
                  if (exit) {
                    _navigationToHome();
                  } else {
                    _navigationToUserForm();
                  }
                })),
                SizedBox(
                  height: 5,
                ),
                // Center(
                //     child: SignInButton(
                //       Buttons.Facebook,
                //       text: "Sign up with Google",
                //       onPressed: () async {
                //         var user = signInWithGoogle();
                //       },
                //     )),
                Spacer(
                  flex: 20,
                )
              ],
            )));
  }
}
