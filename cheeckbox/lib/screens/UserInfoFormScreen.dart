part of 'Screens.dart';

class UserInfoFormScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
            child: Container(child: Body()),
            // behavior: HitTestBehavior.opaque,
            onTap: () {
              FocusScope.of(context).unfocus();
            }));
  }
}

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  // final nameFocus = FocusNode();
  final usernameController = TextEditingController();

  DateTime birthday = DateTime(2000);
  String gender = "Male";
  String username = "noName";
  var _currentPosition;
  var _currentAddress;
  String _country = "";
  var locality;
  var geoPoint;
  String nationality ="";

  String getDate(DateTime time) {
    String date = DateFormat("yyyy/MM/dd").format(time);
    return date;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    usernameController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  Future? _navigationToHome() {
    return Get.to(
      Home(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  Future? _navigationToIntro() {
    return Get.to(
      OnBoardingPage(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  _getCurrentLocation() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        _getAddressFromLatLng();
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      print(placemarks);
      Placemark place = placemarks[0];

      setState(() {
        locality = place.locality;
        _currentAddress = "${place.locality}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = displayHeight(context);
    double width = displayWidth(context);
    final theme = Theme.of(context);

    return SafeArea(
      child: Column(
        children: [
          Spacer(
            flex: 2,
          ),
          Container(
              child: Text(
            "Put Your Information!",
            style: theme.textTheme.headline2,
          )),
          Spacer(
            flex: 1,
          ),
          Container(
              width: width * 0.9,
              child: TextFormField(
                decoration: InputDecoration(hintText: "Username"),
                controller: usernameController,
              )),
          Container(
              child: ListTile(
            trailing: Text(
              getDate(birthday),
              style: theme.textTheme.subtitle2,
            ),
            title: RaisedButton(
              child: Text(
                "Pick Your Birth Day",
              ),
              shape: StadiumBorder(),
              onPressed: () {
                showModalBottomSheet(
                    isDismissible: false,
                    context: context,
                    builder: (BuildContext builder) {
                      return DatePicker(
                        onChange: (int) {},
                        pickertitle: "Your Birth Day",
                      );
                    }).then((value) => {
                      setState(() {
                        print(value);
                        if (value != null) {
                          birthday = value;
                        }
                      })
                    });
              }, //@
            ),
          )),
          Container(
              child: ListTile(
            trailing: Text(
              gender,
              style: theme.textTheme.subtitle2,
            ),
            title: RaisedButton(
              child: Text(
                "Pick Your Gender",
              ),
              shape: StadiumBorder(),
              onPressed: () {
                showModalBottomSheet(
                    isDismissible: false,
                    context: context,
                    builder: (BuildContext builder) {
                      return Picker(
                        pickertitle: "Your Gender",
                      );
                    }).then((value) {
                  setState(() {
                    print(value);
                    if (value != null) {
                      gender = value;
                    }
                  });
                });
              },
            ),
          )),
          // Container(
          //     child: ListTile(
          //   trailing:
          //       _currentAddress != null ? Text(_currentAddress) : Text(""),
          //   title: RaisedButton(
          //     child: Text(
          //       "Get Your Current Live",
          //     ),
          //     shape: StadiumBorder(),
          //     onPressed: () async {
          //       await _getCurrentLocation();
          //       print(_currentAddress);
          //     },
          //   ),
          // )),
          Container(
              child: ListTile(
                title: RaisedButton(
                  child: Text(
                    "Get Your Nationality",
                  ),
                  shape: StadiumBorder(),
                  onPressed: () async {
                    showCountryPicker(
                      countryListTheme: CountryListThemeData(
                        inputDecoration:  InputDecoration(
                          icon: Icon(Icons.search),
                             border: theme.inputDecorationTheme.border,
                            filled: theme.inputDecorationTheme.filled,
                          fillColor: theme.inputDecorationTheme.fillColor,
                          contentPadding: theme.inputDecorationTheme.contentPadding,
                          hintText: "Search"
                        )
                      ),
                      context: context,
                      showPhoneCode: false, // optional. Shows phone code before the country name.
                      onSelect: (Country country) {
                        print('Select country: ${country.countryCode}');
                        setState(() {
                          nationality = country.name;
                          _country = country.countryCode;
                        });

                      },
                    );
                  },
                ),
                trailing: Text(nationality),
              )),
          Spacer(
            flex: 20,
          ),
          Container(
            width: width - 30,
            height: height / 20,
            child: RaisedButton(
              child: Text(
                "Les's Survey!!",
              ),
              shape: StadiumBorder(),
              onPressed: () {
                if (usernameController.text == "" ||
                    !birthday.isBefore(DateTime.now())) {
                  print("problem");
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          backgroundColor: theme.primaryColor,
                          title: new Text("Put Your Information Again"),
                          content: new Text(
                              "There is missing or wrong information."),
                          actions: <Widget>[
                            new TextButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Get.back();
                              },
                            ),
                          ],
                        );
                      });
                } else {
                  // geoPoint = GeoPoint(
                  //     _currentPosition.latitude, _currentPosition.longitude);
                  // _navigationToIntro();
                  username = usernameController.text;
                  createUser(
                          username,
                          birthday,
                          gender,
                          FirebaseAuth.instance.currentUser!.uid, _country)
                      .then((error) {
                    if (error == null) {
                      _navigationToHome();
                    } else {
                      // Get.back();
                    }
                  });
                }
              },
            ),
          ),

          Spacer(
            flex: 2,
          )
        ],
      ),
    );
  }

  Widget setupAlertDialoadContainer() {
    return Container(
      height: 300.0, // Change as per your requirement
      width: 300.0, // Change as per your requirement
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text('Gujarat, India'),
          );
        },
      ),
    );
  }
}
