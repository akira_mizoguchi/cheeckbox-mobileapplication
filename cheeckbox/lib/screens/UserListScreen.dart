part of 'Screens.dart';

class UserListScreen extends StatefulWidget {
  final String uid;

  const UserListScreen({Key? key, required this.uid}) : super(key: key);

  @override
  _UserListScreenState createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {
  String username = "";

  Future getUserInfo() async {
    FirebaseFirestore.instance
        .collection("users")
        .doc(widget.uid)
        .get()
        .then((snapshot) {
      AppUser user = AppUser.fromJson(snapshot.data() as Map<String, dynamic>);
      setState(() {
        username = user.username;
      });
    }).catchError((e) {});
  }

  Future? _navigationToUserProfile() {
    return Get.to(
      UserProfileScreen(uid: widget.uid),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  initState() {
    getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              title: Text(
                "@${username}",
              ),
              bottom: TabBar(
                tabs: [
                  Tab(
                    text: "Follower",
                  ),
                  Tab(
                    text: "Follow",
                  ),
                ],
              ),
            ),
            body: TabBarView(children: [
              FutureUserListView(
                future: getFollowerUser(widget.uid),
              ),
              FutureUserListView(
                future: getFollowUser(widget.uid),
              )
            ])));
  }
}
