part of 'Screens.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({
    Key? key,
  }) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with SingleTickerProviderStateMixin {
  final searchTextController = TextEditingController();
  String usernameText = "";
  String tagText = "";
  late TabController _controller;
  int _selectedIndex = 0;

  final List<Tab> myTabs = <Tab>[
    Tab(
      text: "#Tag",
    ),
    Tab(
      text: "User",
    ),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Create TabController for getting the index of current tab
    _controller = TabController(length: myTabs.length, vsync: this);

    _controller.addListener(() {
      setState(() {
        _selectedIndex = _controller.index;
        print("Selected Index: " + _controller.index.toString());
      });
    });
  }

  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool isScrolled) {
            return [
              CupertinoSliverNavigationBar(
                backgroundColor: theme.primaryColor,
                automaticallyImplyLeading: false,
                largeTitle: Text(
                  "Search",
                  style: TextStyle(color: theme.textTheme.headline1!.color),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                    child: CupertinoSearchTextField(
                      style: TextStyle(color: Colors.black),
                      itemSize: 30,
                      controller: searchTextController,
                      onSubmitted: (String) {
                        setState(() {
                          if (_controller.index == 1) {
                            usernameText = String;
                          } else if (_controller.index == 0) {
                            tagText = String;
                          }
                        });
                      },
                    )),
              ),
              SliverPersistentHeader(
                  pinned: true,
                  delegate: SliverPersistentHeaderA(
                      tabBar: TabBar(
                    controller: _controller,
                    unselectedLabelColor: Colors.grey,
                    tabs: myTabs,
                  ))),
            ];
          },
          body: TabBarView(
            controller: _controller,
            children: [
              FuturePostListView(
                future: getSurveysByTag(tagText),
              ),
              FutureUserListView(
                future:
                    getUserByUsername(usernameText) as Future<QuerySnapshot>,
              )
            ],
          ),
        ));
  }
}
