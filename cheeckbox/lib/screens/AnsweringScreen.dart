part of 'Screens.dart';

class AnsweringScreen extends StatefulWidget {
  final questionId;

  const AnsweringScreen({this.questionId, Key? key}) : super(key: key);

  @override
  _AnsweringScreenState createState() => _AnsweringScreenState();
}

class _AnsweringScreenState extends State<AnsweringScreen> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: FutureUserFlexibleSpace(
            future: getQuestionCreatorId(widget.questionId),
          ),
        ),
        body: FutureBuilder(
            future: Future.wait([
              getSurveyById(widget.questionId),
              getUserAnswered(widget.questionId)
            ]),
            builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
              if (snapshot.hasError) {
                return Text("Something went wrong");
              }
              if (snapshot.connectionState == ConnectionState.done) {
                Question question =
                    Question.fromJsonFireStore(snapshot.data![0].data());
                var userAnsweredData = snapshot.data![1].data();
                return AnsweringView(
                  userAnsweredData: userAnsweredData,
                  question: question,
                );
              }
              return Container();
            }));
  }
}

class AnsweringView extends StatefulWidget {
  final Question question;
  final userAnsweredData;

  const AnsweringView(
      {required this.userAnsweredData, Key? key, required this.question})
      : super(key: key);

  @override
  _AnsweringViewState createState() => _AnsweringViewState();
}

class _AnsweringViewState extends State<AnsweringView> {
  late var selectedIndex;
  Color selectedOption = Colors.blue;
  int votedNum = 0;
  int commentNum = 0;
  bool answered = false;
  bool showComment = true;
  Icon seeAndClose = Icon(Icons.arrow_drop_down);

  void getVotedNum() {
    FirebaseFirestore.instance
        .collection("Surveys")
        .doc(widget.question.questionId)
        .collection("Answers")
        .get()
        .then((snapshot) {
      setState(() {
        votedNum = snapshot.size;
      });
    }).catchError((e) {
      votedNum = 0;
    });
  }

  void getCommentNum() {
    FirebaseFirestore.instance
        .collection("Surveys")
        .doc(widget.question.questionId)
        .collection("Comments")
        .get()
        .then((snapshot) {
      setState(() {
        commentNum = snapshot.size;
      });
    }).catchError((e) {
      commentNum = 0;
    });
  }

  @override
  void initState() {
    if (widget.userAnsweredData != null) {
      answered = true;
      selectedIndex = widget.userAnsweredData["selected_index"];
    } else {
      selectedIndex = null;
    }
    getCommentNum();
    getVotedNum();
    super.initState();
  }

  Future<void> _reload() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    Future getcomment = getComments(widget.question.questionId);
    return RefreshIndicator(
        onRefresh: _reload,
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  ContentContainer(
                    question: widget.question,
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(30)),
                      padding: EdgeInsets.only(left: 5, right: 10),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 20,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(votedNum.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold)),
                          Text(
                            " Answered",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),

                          // Text("pinned",style: theme.textTheme.caption,),
                          // IconButton(icon: Icon(Icons.playlist_add_outlined,color: Colors.grey,size: 30,),)
                        ],
                      )),
                  Divider(),
                  Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          // color: Colors.black,
                          borderRadius: BorderRadius.circular(20)),
                      child: Text(
                        "Choice",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      )),

                  ///Options List
                ],
              ),
            ),
            SliverList(
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return MyFadeTransition(
                    duration: Duration(milliseconds: 1000 + index * 100),
                    child: Padding(
                        padding:
                            EdgeInsets.only(left: 10, right: 10, bottom: 2),
                        child: Card(
                            elevation: 0.8,
                            color:
                                selectedIndex == index ? Colors.white70 : null,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18)),
                            child: ListTile(
                              leading: Text(
                                "${index + 1}",
                                style: theme.textTheme.headline5,
                              ),
                              title: Text(
                                widget.question.choices[index],
                                style: theme.textTheme.headline6,
                                textAlign: TextAlign.start,
                              ),
                              onTap: () {
                                answeringQuestion(
                                    widget.question.questionId,
                                    index,
                                    FirebaseAuth.instance.currentUser!.uid,
                                    widget.question.choices[index]);
                                setState(() {
                                  selectedIndex = index;
                                  answered = true;
                                });
                              },
                            ))));
              }, childCount: widget.question.choices.length),
            ),

            SliverToBoxAdapter(
              child: Divider(),
            ),

            ///Graph
            answered
                ? SliverToBoxAdapter(
                    child: FutureBarChart(
                    questionId: widget.question.questionId,
                  ))
                : SliverToBoxAdapter(),
            //
            ///Comment
            answered
                ? SliverToBoxAdapter(
                    child: Column(
                    children: [
                      Divider(
                        color: dividerColor,
                      ),

                      ///Comment
                      ListTile(
                        title: Row(
                          children: <Widget>[
                            Icon(
                              Icons.comment,
                            ),
                            Text(' Comment', style: theme.textTheme.subtitle1),
                          ],
                        ),
                        subtitle: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: commentNum.toString(),
                                  style: theme.textTheme.subtitle2),
                              TextSpan(
                                  text: ' comment',
                                  style: theme.textTheme.caption),
                            ],
                          ),
                        ),
                      ),
                      Divider(
                        color: dividerColor,
                      ),

                      ///CommentInput
                      CommentInput(
                        questionId: widget.question.questionId,
                      ),
                      Divider(
                        color: dividerColor,
                      ),
                    ],
                  ))
                : SliverToBoxAdapter(),
            if (answered)
              FutureSliverComments(
                questionId: widget.question.questionId,
                future: getcomment as Future<QuerySnapshot<Object>>,
              )

            // SliverToBoxAdapter(child: Text("iii"),)
            else
              SliverToBoxAdapter(),
            SliverToBoxAdapter(
                child: SizedBox(
              height: 20,
            ))
          ],
        ));
  }
}
