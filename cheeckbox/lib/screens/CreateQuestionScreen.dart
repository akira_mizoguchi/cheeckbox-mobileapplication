part of 'Screens.dart';

class CreateQuestionScreen extends StatefulWidget {
  const CreateQuestionScreen({Key? key}) : super(key: key);

  @override
  _CreateQuestionScreenState createState() => _CreateQuestionScreenState();
}

class _CreateQuestionScreenState extends State<CreateQuestionScreen> {
  final optionTextController = new TextEditingController();
  final URLTextController = new TextEditingController();
  final TagTextController = new TextEditingController();
  final questionTextController = new TextEditingController();
  final focusNode = FocusNode();

  List<String> listItems = [];
  List<String> tags = [];

  String nationality = "";
  String _country = "";

  Future? _navigationToHome() {
    return Get.off(
      Home(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 500),
    );
  }

  void reorderData(int oldIndex, int newIndex) {
    setState(() {
      if (newIndex > oldIndex) {
        newIndex -= 1;
      }
      final items = listItems.removeAt(oldIndex);
      listItems.insert(newIndex, items);
      print(listItems);
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return GestureDetector(
      onTap: () {
        focusNode.unfocus();
      },
      child: Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                  padding: EdgeInsets.only(left: 10),
                  alignment: Alignment.center,
                  child: Text(
                    "Cancel",
                    style: TextStyle(color: Colors.blue),
                  )),
            ),
            actions: [
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Material(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.blue,
                      child: new InkWell(
                          customBorder: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          onTap: () {
                            if (questionTextController.text != null &&
                                listItems.length != 0) {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: new Text("Post Your Survey"),
                                    // content: new Text("You are awesome!"),
                                    content: Text("Are you okay to post?"),
                                    actions: <Widget>[
                                      new TextButton(
                                        child: new Text("Cancel"),
                                        onPressed: Get.back,
                                      ),
                                      new TextButton(
                                        child: new Text("Post"),
                                        onPressed: () async {
                                          var result = await createQuestion(
                                              questionTextController.text,
                                              tags,
                                              FirebaseAuth
                                                  .instance.currentUser!.uid,
                                              listItems,
                                              URLTextController.text,_country)
                                          .onError((error, stackTrace) =>
                                              {_navigationToHome()})
                                          .whenComplete(
                                              () => _navigationToHome());
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            } else {}
                          },
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                "Upload",
                                style: theme.textTheme.bodyText1!
                                    .copyWith(color: Colors.white),
                              )))))
            ],
          ),
          body: Builder(builder: (BuildContext context) {
            return ReorderableListView(
              shrinkWrap: true,
              header: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Question",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        focusNode: focusNode,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Write Question..."),
                        maxLines: 5,
                        controller: questionTextController,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "URL",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Here is the link..."),
                        maxLines: null,
                        controller: URLTextController,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Tag #",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextFieldTags(
                          tagsStyler: TagsStyler(
                              showHashtag: true,
                              tagTextStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                              tagDecoration: BoxDecoration(
                                color: Colors.blue[300],
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              tagCancelIcon: Icon(Icons.cancel,
                                  size: 18.0, color: Colors.blue[900]),
                              tagPadding: const EdgeInsets.all(6.0)),
                          textFieldStyler: TextFieldStyler(
                              textFieldFilled: true,
                              textFieldBorder: InputBorder.none,
                              textFieldFilledColor:
                                  theme.inputDecorationTheme.fillColor,
                              hintText: "Type your tags...",
                              helperText: "less than 15 chars",
                              helperStyle: TextStyle(color: Colors.grey),
                              hintStyle: TextStyle(color: Colors.grey)),
                          onTag: (tag) {
                            tags.add(tag);
                            print(tags);
                          },
                          onDelete: (tag) {
                            tags.remove(tag);
                            print(tags);
                          },
                          validator: (tag) {
                            if (tag!.length > 15) {
                              return "should be less than 15";
                            }
                            return null;
                          })),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Content Country",
                        style: theme.textTheme.headline5,
                      )),
                  ListTile(
                    trailing: Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.grey
                      ),
                        child: Text(nationality,style: theme.textTheme.bodyText2!.copyWith(color: Colors.white),)
                    ),
                      title: OutlineButton(
                          textColor: Colors.grey,
                          onPressed: () {
                            showCountryPicker(
                              countryListTheme: CountryListThemeData(
                                  inputDecoration: InputDecoration(
                                      icon: Icon(Icons.search),
                                      border: theme.inputDecorationTheme.border,
                                      filled: theme.inputDecorationTheme.filled,
                                      fillColor:
                                          theme.inputDecorationTheme.fillColor,
                                      contentPadding: theme
                                          .inputDecorationTheme.contentPadding,
                                      hintText: "Search")),
                              context: context,
                              showPhoneCode: false,
                              // optional. Shows phone code before the country name.
                              onSelect: (Country country) {
                                print('Select country: ${country.countryCode}');
                                setState(() {
                                  nationality = country.name;
                                  _country = country.countryCode;
                                });
                              },
                            );
                          },
                          child: Text("Pick Country"))),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Choice",
                        style: theme.textTheme.headline5,
                      )),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                            hintStyle: TextStyle(color: Colors.grey),
                            hintText: "Write Choice..."),
                        maxLines: null,
                        controller: optionTextController,
                      )),
                ],
              ),
              children: <Widget>[
                for (final items in listItems)
                  Padding(
                      key: Key(items),
                      padding: EdgeInsets.all(5),
                      child: MyFadeTransition(
                          child: Card(
                        elevation: 0.3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                        child: ListTile(
                          title: Padding(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: Text(items)),
                          leading: Text("${listItems.indexOf(items) + 1}"),
                          trailing: IconButton(
                            icon: Icon(
                              Icons.delete,
                              color: Colors.grey,
                            ),
                            onPressed: () {
                              setState(() {
                                listItems.remove(items);
                                print(listItems);
                              });
                            },
                          ),
                        ),
                      ))),
                SizedBox(
                  height: 100,
                  key: Key("height"),
                )
              ],
              onReorder: reorderData,
            );
          }),
          floatingActionButton: Material(
              borderRadius: BorderRadius.circular(20),
              color: Colors.blue,
              child: new InkWell(
                  customBorder: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  onTap: () {
                    setState(() {
                      listItems.add(optionTextController.text);
                      optionTextController.text = "";
                      print(listItems);
                    });
                  },
                  child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Choice+",
                        style: theme.textTheme.bodyText1!
                            .copyWith(color: Colors.white,fontSize: 16),
                      ))))
          ),
    );
  }
}
