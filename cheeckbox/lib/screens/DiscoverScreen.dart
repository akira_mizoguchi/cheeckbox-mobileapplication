part of 'Screens.dart';

class DiscoverScreen extends StatefulWidget {
  const DiscoverScreen({Key? key}) : super(key: key);

  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  Future? _navigationToAnsweringScreen() {
    return Get.to(
      AnsweringScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 500),
    );
  }

  Future? _navigationToCreateQuestion() {
    return Get.to(
      CreateQuestionScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final List<Tab> myTabs = <Tab>[
      Tab(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Text("Trend"),
          ),
        ),
      ),
    ];
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: _navigationToCreateQuestion,
          elevation: 10,
          backgroundColor: Colors.blue,
          child: Icon(Icons.add),
        ),
        body: DefaultTabController(
            length: 1,
            child: NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool isScrolled) {
                return [
                  CupertinoSliverNavigationBar(
                    backgroundColor: theme.primaryColor,
                    automaticallyImplyLeading: false,
                    largeTitle: Container(
                        child: Text(
                      "Discover",
                      style: TextStyle(color: theme.textTheme.headline1!.color),
                    )),
                  ),
                ];
              },
              body: TabBarView(
                children: [TrendScreen()],
              ),
            )));
  }
}
