part of "Screens.dart";

class EditScreen extends StatelessWidget {
  const EditScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getUserbyUid(FirebaseAuth.instance.currentUser!.uid),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          AppUser user = AppUser.fromJson(snapshot.data.data());
          return EditScreenBody(
            user: user,
          );
        } else {
          return Text("");
        }
      },
    );
  }
}

class EditScreenBody extends StatefulWidget {
  final AppUser user;

  const EditScreenBody({Key? key, required this.user}) : super(key: key);

  @override
  _EditScreenBodyState createState() => _EditScreenBodyState();
}

class _EditScreenBodyState extends State<EditScreenBody> {
  final nameTextController = TextEditingController();
  final usernameTextController = TextEditingController();
  final bioTextController = TextEditingController();
  late String imageURL;
  double _progress = 0;

  var _image;
  final picker = ImagePicker();

  final focusNode = FocusNode();

  Future _getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  Future uploadImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        setState(() {
          _image = File(pickedFile.path);
        });
      }
    });
    print("done to pick picture");
    var firebaseStorageRef = firebase_storage.FirebaseStorage.instance
        .ref('Users/${FirebaseAuth.instance.currentUser!.uid}/profile.png');
    var uploadTask = firebaseStorageRef.putFile(_image);
    print("after upload");
    uploadTask.snapshotEvents.listen((firebase_storage.TaskSnapshot snapshot) {
      print('Task state: ${snapshot.state}');
      setState(() {
        _progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      });
      print(
          'Progress: ${(snapshot.bytesTransferred / snapshot.totalBytes) * 100} %');
    }, onError: (e) {
      // The final snapshot is also available on the task via `.snapshot`,
      // this can include 2 additional states, `TaskState.error` & `TaskState.canceled`
      print(uploadTask.snapshot);

      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
    });
    try {
      var task = await uploadTask;
      var url = await task.ref.getDownloadURL();
      if (url.isNotEmpty) {
        await FirebaseFirestore.instance
            .collection("Users")
            .doc(FirebaseAuth.instance.currentUser!.uid)
            .update({"profileURL": url}).then((value) {
          setState(() {
            imageURL = widget.user.profileURL;
          });
        });
      } else {
        print("error when upload on FireStore");
      }
      print('Upload complete.');
    } on FirebaseException catch (e) {
      if (e.code == 'permission-denied') {
        print('User does not have permission to upload to this reference.');
      }
      // ...
    }
  }

  Future? _navigationToWelcomeScreen() {
    return Get.off(
      () => WelcomeScreen(),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 1500),
    );
  }

  @override
  void initState() {
    nameTextController.text = widget.user.name;
    usernameTextController.text = "${widget.user.username}";
    bioTextController.text = widget.user.bio;
    imageURL = widget.user.profileURL;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return GestureDetector(
        onTap: () {
          focusNode.unfocus();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: 10),
                  child: Text("Back")),
            ),
            title: Text("Edit Profile"),
            actions: [
              GestureDetector(
                onTap: () async {
                  await updateUserInfo(widget.user.uid, nameTextController.text,
                          usernameTextController.text, bioTextController.text)
                      .then((value) {
                    Get.back();
                  }).catchError((e) {
                    print(e);
                    Get.back();
                  });
                },
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(right: 15),
                    child: Text(
                      "Save",
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    )),
              ),
            ],
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _progress > 0 && _progress < 1
                  ? LinearProgressIndicator(
                      value: _progress,
                    )
                  : Container(),

              ///Profile Avatar
              Center(
                child: Container(
                    // padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blue.withOpacity(0.4)),
                    child: CachedNetworkImage(
                      imageUrl: imageURL,
                      imageBuilder: (context, imageProvider) => CircleAvatar(
                        foregroundImage:
                            _image != null ? FileImage(_image) : null,
                        backgroundImage: imageProvider,
                        minRadius: 25,
                        maxRadius: 50,
                      ),
                      placeholder: (context, url) => CircleAvatar(
                        radius: 30,
                      ),
                      errorWidget: (context, url, error) => CircleAvatar(
                        radius: 30,
                        backgroundImage: AssetImage('assets/user.png'),
                      ),
                    )),
              ),
              SizedBox(
                height: 10,
              ),
              Flexible(
                  child: Material(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.blue,
                      child: new InkWell(
                          customBorder: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          onTap: uploadImage,
                          child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                "Change Profile",
                                style: theme.textTheme.bodyText1!
                                    .copyWith(color: Colors.white),
                              )))
                  )
              ),
              SizedBox(
                height: 10,
              ),
              Divider(
                color: dividerColor,
              ),

              ///UserNmae
              ListTile(
                leading: Text(
                  "Name:",
                  style: theme.textTheme.bodyText1,
                ),
                title: TextField(
                  controller: nameTextController,
                  decoration: InputDecoration(),
                ),
              ),
              Divider(
                color: dividerColor,
              ),

              ///UserId
              ListTile(
                leading: Text(
                  "UserName:",
                  style: theme.textTheme.bodyText1,
                ),
                title: TextField(
                  decoration: InputDecoration(
                      hintStyle: TextStyle(color: Colors.grey),
                      hintText: "username"),
                  controller: usernameTextController,
                ),
              ),
              Divider(
                color: dividerColor,
              ),
              ListTile(
                leading: Text(
                  "Bio:",
                  style: theme.textTheme.bodyText1,
                ),
                title: TextField(
                  focusNode: focusNode,
                  minLines: 5,
                  maxLines: 7,
                  decoration: InputDecoration(
                      hintStyle: TextStyle(color: Colors.grey),
                      hintText: "Write your biography"),
                  controller: bioTextController,
                ),
              ),
              Spacer(),
              Container(
                alignment: Alignment.bottomCenter,
                child: OutlineButton(
                  child: Text("Logout",
                      style: theme.textTheme.headline5!
                          .copyWith(color: Colors.grey)),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            backgroundColor: theme.primaryColor,
                            title: new Text("Log out?"),
                            content:
                                new Text("Are you sure you want to log out?"),
                            actions: <Widget>[
                              new TextButton(
                                child: new Text("Cancel"),
                                onPressed: () async {
                                  Get.back();
                                },
                              ),
                              new TextButton(
                                child: new Text("OK"),
                                onPressed: () async {
                                  FirebaseAuth.instance.signOut();
                                  _navigationToWelcomeScreen();
                                },
                              ),
                            ],
                          );
                        });
                  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.bottomCenter,
                child: OutlineButton(
                  // borderSide: BorderSide(color: Colors.black),
                  child: Text("Delete Account",
                      style: theme.textTheme.headline5!
                          .copyWith(color: Colors.grey)),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            backgroundColor: theme.primaryColor,
                            title: new Text("Delete your account ?"),
                            content: new Text(
                                "Are you sure you want to delete your account?"),
                            actions: <Widget>[
                              TextButton(
                                child: new Text(
                                  "cancel",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                onPressed: () async {
                                  Get.back();
                                },
                              ),
                              TextButton(
                                child: new Text(
                                  "OK",
                                  style: TextStyle(color: Colors.red),
                                ),
                                onPressed: () async {
                                  var uid =
                                      FirebaseAuth.instance.currentUser!.uid;
                                  bool result_firestorage =
                                      await deleteProfilePic(uid);
                                  bool result_firestore =
                                      await deleteAccount(uid);
                                  await _navigationToWelcomeScreen();
                                },
                              ),
                            ],
                          );
                        });
                  },
                ),
              ),
              // SizedBox(height: 10,)
            ],
          ),
        ));
  }
}
