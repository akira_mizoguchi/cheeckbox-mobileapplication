part of 'localizations.dart';

class AppLocalization {
  final Locale locale;
  late Map<String, String> _localizedValues;
  static const LocalizationsDelegate<AppLocalization> delegate =
  _DemoLocalizationDelegate();

  AppLocalization(this.locale);



  static AppLocalization? of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }



  Future load() async {
    String jsonStringValues =
        await rootBundle.loadString('lib/lang/${locale.languageCode}.json');

    Map<String, dynamic> mappedJson = json.decode(jsonStringValues);

    _localizedValues =
        mappedJson.map((key, value) => MapEntry(key, value.toString()));
  }

  String? getTranslateValue(String key) {
    return _localizedValues[key];
  }


}

class _DemoLocalizationDelegate
    extends LocalizationsDelegate<AppLocalization> {
  const _DemoLocalizationDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'es',"ja"].contains(locale.languageCode);
  }

  @override
  Future<AppLocalization> load(Locale locale) async {
    AppLocalization localization = AppLocalization(locale);
    await localization.load();
    return localization;
  }

  @override
  bool shouldReload(_DemoLocalizationDelegate old) {
    return false;
  }
}
