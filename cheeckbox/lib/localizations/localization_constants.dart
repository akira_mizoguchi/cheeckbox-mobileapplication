part of 'localizations.dart';

String? getTranslated(BuildContext context, String key) {
  return AppLocalization.of(context)!.getTranslateValue(key);
}
