part of 'widgets.dart';

class FuturePostListView extends StatefulWidget {
  final Future<QuerySnapshot> future;

  const FuturePostListView({Key? key, required this.future}) : super(key: key);

  @override
  _FuturePostListViewState createState() => _FuturePostListViewState();
}

class _FuturePostListViewState extends State<FuturePostListView> {
  Future<void> _reload() async {
    setState(() {
      widget.future;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Widget noContext() {
      return Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          "No Content",
          style: theme.textTheme.headline3,
        ),
        Text(
          "Find Surveys You Want",
          style: theme.textTheme.bodyText2,
        )
      ]));
    }

    return RefreshIndicator(
        onRefresh: _reload,
        child: FutureBuilder(
          future: widget.future,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              //when the data has no data (size == 0) return nonContext()
              if (snapshot.data == null || snapshot.data!.size == 0) {
                return noContext();
              } else {
                return ListView(
                  children: snapshot.data!.docs.map(
                    (DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data() as Map<String, dynamic>;
                      final Question question =
                          Question.fromJsonFireStore(data);
                      return MyFadeTransition(
                          child: Post(
                        question: question,
                      ));
                    },
                  ).toList(),
                );
              }
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              //this is return when the snapshot will return when there is no has data
              return noContext();
            }
          },
        ));
  }
}
