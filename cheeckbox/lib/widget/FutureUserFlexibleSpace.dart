part of 'widgets.dart';

class FutureUserFlexibleSpace extends StatelessWidget {
  final Future future;

  const FutureUserFlexibleSpace({Key? key, required this.future})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          final AppUser user = AppUser.fromJson(data);
          return MyFadeTransition(
              child: UserFlexibleSpace(
            user: user,
          ));
        }
        return Text("");
      },
    );
  }
}
