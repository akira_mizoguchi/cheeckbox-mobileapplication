part of 'widgets.dart';

class FutureOwnPostListView extends StatefulWidget {
  final Future<QuerySnapshot> future;

  const FutureOwnPostListView({Key? key, required this.future})
      : super(key: key);

  @override
  _FutureOwnPostListViewState createState() => _FutureOwnPostListViewState();
}

class _FutureOwnPostListViewState extends State<FutureOwnPostListView> {
  Future<void> _reload() async {
    setState(() {
      widget.future;
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Widget noContext() {
      return Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          "No Content",
          style: theme.textTheme.headline3,
        ),
        Text(
          "Find Survey You Want",
          style: theme.textTheme.bodyText2,
        )
      ]));
    }

    return RefreshIndicator(
        onRefresh: _reload,
        child: FutureBuilder(
          future: widget.future,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            //when the snapshot has data

            if (snapshot.hasData) {
              //when the size of data is 0
              if (snapshot.data == null || snapshot.data!.size == 0) {
                return noContext();
              }
              //when the data has data and has the size more than 1
              else {
                return ListView(
                    children: snapshot.data!.docs.map(
                  (DocumentSnapshot document) {
                    Map<String, dynamic> data =
                        document.data() as Map<String, dynamic>;
                    final Question question = Question.fromJsonFireStore(data);
                    return MyFadeTransition(
                        child: OwnPost(
                      question: question,
                    ));
                  },
                ).toList());
              }
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              return noContext();
            }
          },
        ));
  }
}
