part of 'widgets.dart';

class ContentContainer extends StatefulWidget {
  final Question question;

  const ContentContainer({Key? key, required this.question}) : super(key: key);

  @override
  _ContentContainerState createState() => _ContentContainerState();
}

class _ContentContainerState extends State<ContentContainer> {
  Map<String, PreviewData> datas = {};

  Future? _navigationToWebView() {
    return Get.to(
      WebViewScreen(
        URL: widget.question.URL,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 10, right: 10),
          alignment: Alignment.centerLeft,
          child: Text(
            widget.question.questionText == null
                ? ""
                : widget.question.questionText,
            style: theme.textTheme.headline5,
          ),
        ),
        widget.question.URL != ""
            ?
            // Container(
            //     padding: EdgeInsets.all(20),
            //     alignment: Alignment.centerLeft,
            //     child: GestureDetector(
            //         onTap: _navigationToWebView,
            //         child: RichText(
            //           text: TextSpan(
            //             children: <TextSpan>[
            //               TextSpan(
            //                   text: 'URL: ', style: theme.textTheme.caption),
            //               TextSpan(
            //                   text: widget.question.URL,
            //                   style:
            //                       TextStyle(color: Colors.blue, fontSize: 15)),
            //             ],
            //           ),
            //         )),
            //   )
            Container(
                // key: ValueKey(urls[0]),
                margin: const EdgeInsets.all(16),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  color: Color(0xfff7f7f8),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20),
                  ),
                  child: LinkPreview(
                    enableAnimation: true,
                    onPreviewDataFetched: (data) {
                      setState(() {
                        datas = {
                          // ...datas,
                          widget.question.URL: data,
                        };
                      });
                    },
                    previewData: datas[widget.question.URL],
                    text: widget.question.URL,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
              )
            : Container(
                height: 15,
              ),
        widget.question.tags.length == 0
            ? Container()
            : Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.centerLeft,
                child: TagsRow(
                  tags: widget.question.tags,
                ),
              )
      ],
    );
  }
}
