part of 'widgets.dart';

class UserFlexibleSpace extends StatelessWidget {
  final AppUser user;

  const UserFlexibleSpace({Key? key, required this.user}) : super(key: key);

  Future? _navigationToUserProfile() {
    if (user.uid != FirebaseAuth.instance.currentUser!.uid) {
      return Get.to(
        UserProfileScreen(
          uid: user.uid,
        ),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SafeArea(
        child: GestureDetector(
      onTap: () {
        print("tap");
        _navigationToUserProfile();
      },
      child: Container(
        padding: EdgeInsets.only(right: 16),
        child: Row(
          children: <Widget>[
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back,
              ),
            ),
            SizedBox(
              width: 2,
            ),
            Container(
                // padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.blue.withOpacity(0.4)
                ),
                child: CachedNetworkImage(
                    imageUrl: user.profileURL,
                    imageBuilder: (context, imageProvider) => CircleAvatar(
                          backgroundImage: imageProvider,
                        ),
                    placeholder: (context, url) => CircleAvatar(),
                    errorWidget: (context, url, error) => CircleAvatar(
                        backgroundImage: AssetImage('assets/user.png')))),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(user.name, style: theme.textTheme.subtitle1),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    "@${user.username}",
                    style: TextStyle(color: Colors.grey.shade600, fontSize: 13),
                  ),
                ],
              ),
            ),
            // Icon(
            //   Icons.settings,
            // ),
          ],
        ),
      ),
    ));
  }
}

class OwnerFlexibleSpace extends StatelessWidget {
  final AppUser user;

  const OwnerFlexibleSpace({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SafeArea(
      child: Container(
        padding: EdgeInsets.only(right: 16),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),

            CircleAvatar(
              backgroundImage:
                  user.profileURL != "" ? NetworkImage(user.profileURL) : null,
              maxRadius: 20,
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(user.name, style: theme.textTheme.subtitle2),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    "@${user.username}",
                    style: TextStyle(color: Colors.grey.shade600, fontSize: 13),
                  ),
                ],
              ),
            ),
            // Icon(
            //   Icons.settings,
            // ),
          ],
        ),
      ),
    );
  }
}
