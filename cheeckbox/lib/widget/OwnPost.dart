part of 'widgets.dart';

class OwnPost extends StatefulWidget {
  final Question question;

  OwnPost({Key? key, required this.question}) : super(key: key);

  @override
  _OwnPostState createState() => _OwnPostState();
}

class _OwnPostState extends State<OwnPost> {
  int votedNum = 0;

  Future? _navigationToAnsweringScreen() {
    return Get.to(
      AnsweringScreen(
        questionId: widget.question.questionId,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 500),
    );
  }

  Future? _navigationToUserProfile() {
    return Get.to(
      UserProfileScreen(uid: widget.question.creatorId),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  Future? _navigationToEidtQuestionScreen(String questionId) {
    return Get.to(
      EditQuestionScreen(
        questionId: questionId,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  void getVotedNum() {
    FirebaseFirestore.instance
        .collection("Surveys")
        .doc(widget.question.questionId)
        .collection("Answers")
        .get()
        .then((snapshot) {
      print(snapshot.size);
      setState(() {
        votedNum = snapshot.size;
      });
    }).catchError((e) {
      votedNum = 0;
    });
  }

  @override
  initState() {
    getVotedNum();
    super.initState();
  }

  Future<void> _reload() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    double width = displayWidth(context);
    double height = displayHeight(context);
    return RefreshIndicator(
        onRefresh: _reload,
        child: Padding(
            padding: EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  title: Text(
                    "${getDate(widget.question.postTime.toDate())} posted",
                    style: theme.textTheme.caption,
                  ),
                  trailing: IconButton(
                    icon: Icon(
                      Icons.edit,
                      size: 20,
                      color: Colors.grey.withOpacity(0.5),
                    ),
                    onPressed: () {
                      showModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  leading:
                                      new Icon(Icons.photo, color: Colors.grey),
                                  title: new Text('Edit'),
                                  onTap: () async {
                                    Get.back();
                                    await _navigationToEidtQuestionScreen(
                                        widget.question.questionId);
                                  },
                                ),
                                ListTile(
                                  leading: new Icon(Icons.delete_forever,
                                      color: Colors.grey),
                                  title: new Text('Delete'),
                                  onTap: () async {
                                    //close the modal
                                    Get.back();
                                    await showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          backgroundColor: theme.primaryColor,
                                          title: new Text("Alert!!"),
                                          content: new Text(
                                              "Is it okay to delete ?"),
                                          actions: <Widget>[
                                            new FlatButton(
                                              child: new Text("OK"),
                                              onPressed: () async {
                                                await deleteOwnQuestion(
                                                    widget.question.questionId);
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                ),
                                ListTile(),
                              ],
                            );
                          });
                    },
                  ),
                ),
                GestureDetector(
                    onTap: _navigationToAnsweringScreen,
                    child: ContentContainer(
                      question: widget.question,
                    )),
                Container(
                    margin: EdgeInsets.only(left: 5),
                    decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.circular(30)),
                    padding: EdgeInsets.only(left: 5, right: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(votedNum.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                        Text(
                          " Answered",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),

                        // Text("pinned",style: theme.textTheme.caption,),
                        // IconButton(icon: Icon(Icons.playlist_add_outlined,color: Colors.grey,size: 30,),)
                      ],
                    )),
                SizedBox(
                  height: 15,
                ),
                Divider(),
              ],
            )));
  }
}
