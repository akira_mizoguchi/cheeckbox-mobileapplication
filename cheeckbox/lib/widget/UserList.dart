part of 'widgets.dart';

class UserList extends StatelessWidget {
  final AppUser user;

  const UserList({Key? key, required this.user}) : super(key: key);

  Future? _navigationToUserProfile() {
    return Get.to(
      UserProfileScreen(
        uid: user.uid,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
        padding: EdgeInsets.all(0),
        child: ListTile(
          onTap: _navigationToUserProfile,
          leading: Container(
              // padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.blue.withOpacity(0.4)),
              child: CachedNetworkImage(
                imageUrl: user.profileURL,
                imageBuilder: (context, imageProvider) => CircleAvatar(
                  backgroundImage: imageProvider,
                ),
                placeholder: (context, url) => CircularProgressIndicator(),
                errorWidget: (context, url, error) => CircleAvatar(
                    backgroundImage: AssetImage('assets/user.png')),
              )),
          title:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              user.name.toString(),
              style: theme.textTheme.subtitle2,
            ),
            Text(
              "@${user.username}",
              style: theme.textTheme.caption,
            )
          ]),
          trailing: Container(
            decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            padding: EdgeInsets.all(10),
            child: InkWell(
              onTap: _navigationToUserProfile,
              child: Text(
                "See Profile",
                style: theme.textTheme.headline6!
                    .copyWith(color: Colors.white, fontSize: 13),
              ),
            ),
          ),
          // subtitle: Text(
          //   user.bio,
          //   maxLines: 2,
          //   style: theme.textTheme.bodyText1,
          // ),
        ));
  }
}
