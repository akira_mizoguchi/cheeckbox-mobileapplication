part of 'widgets.dart';

class BarResultChart extends StatelessWidget {
  final List<int> eachTotal;

  const BarResultChart({Key? key, required this.eachTotal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Card(
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Text(
                'Total ${calcTotal(eachTotal)}',
                style: theme.textTheme.headline3,
              )),
          Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                children: [
                  Text(
                    'All',
                    style: TextStyle(
                        color: const Color(0xff379982),
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )),
          Container(
              alignment: Alignment.center,
              height: displayHeight(context) / 3 + 100,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: eachTotal.length,
                itemBuilder: (context, index) {
                  return MyFadeTransition(
                      duration: Duration(microseconds: index * 5),
                      child: ChartBar(
                        total: calcTotal(eachTotal),
                        indexTotal: eachTotal[index],
                        index: index,
                      ));
                },
              )),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}

class ChartBar extends StatelessWidget {
  final int total;
  final int indexTotal;
  final int index;

  const ChartBar(
      {Key? key,
      required this.index,
      required this.total,
      required this.indexTotal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double percent = indexTotal / total.floor();
    //back bar
    return Column(children: [
      TopBarDetail(
        indexTotal: indexTotal,
      ),
      MiddleBarDetail(
        percentage: percent,
        index: index,
      ),
      BottomBarDetail(
        percentage: percent,
        index: index,
      )
    ]);
  }
}

class TopBarDetail extends StatelessWidget {
  final int indexTotal;

  const TopBarDetail({Key? key, required this.indexTotal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(5),
        child: Column(
          children: [
            Text(indexTotal.toString(),
                style: TextStyle(fontWeight: FontWeight.bold)),
            Text("people",
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.grey)),
          ],
        ));
  }
}

class MiddleBarDetail extends StatelessWidget {
  final int index;
  final double percentage;

  const MiddleBarDetail(
      {Key? key, required this.percentage, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = displayHeight(context) / 3;
    final width = displayWidth(context);
    return Container(
        alignment: Alignment.bottomCenter,
        height: height,
        width: width / 10,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.grey.withOpacity(0.1),
        ),
        //inside bar
        child: MyFadeTransition(
            axis: Axis.vertical,
            duration: Duration(milliseconds: 1000 + index),
            child: Container(
              alignment: Alignment.bottomCenter,
              height: height * percentage,
              width: width / 10,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.blue),
            )));
  }
}

class BottomBarDetail extends StatelessWidget {
  final double percentage;
  final int index;

  const BottomBarDetail(
      {Key? key, required this.percentage, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
        padding: EdgeInsets.all(5),
        child: Column(
          children: [
            Text("${(percentage * 100).toInt()}%",
                style: TextStyle(fontWeight: FontWeight.bold)),
            Text(
              "${index + 1} Choice",
              softWrap: true,
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ));
  }
}
