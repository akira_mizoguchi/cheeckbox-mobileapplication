part of 'widgets.dart';

class FutureUserListView extends StatefulWidget {
  final Future<QuerySnapshot> future;

  const FutureUserListView({Key? key, required this.future}) : super(key: key);

  @override
  _FutureUserListViewState createState() => _FutureUserListViewState();
}

class _FutureUserListViewState extends State<FutureUserListView> {
  Future<void> _reload() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Widget noContext() {
      return Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          "No User",
          style: theme.textTheme.headline3,
        ),
        Text(
          "Find User You Want",
          style: theme.textTheme.bodyText2,
        )
      ]));
    }

    return RefreshIndicator(
        onRefresh: _reload,
        child: FutureBuilder(
          future: widget.future,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data!.size == 0) {
                return noContext();
              } else {
                return ListView.separated(
                    itemCount: snapshot.data!.docs.length,
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemBuilder: (context, index) {
                      final AppUser user = AppUser.fromJson(
                          snapshot.data!.docs[index].data()
                              as Map<String, dynamic>);
                      if (FirebaseAuth.instance.currentUser!.uid != user.uid) {
                        return MyFadeTransition(
                            child: UserList(
                          user: user,
                        ));
                      } else {
                        return Container();
                      }
                    });
              }
            } else if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return noContext();
            }
          },
        ));
  }
}
