part of 'widgets.dart';

class CommentContainer extends StatefulWidget {
  final Comment comment;
  final String questionId;
  const CommentContainer({Key? key, required this.comment, required this.questionId}) : super(key: key);

  @override
  _CommentContainerState createState() => _CommentContainerState();
}

class _CommentContainerState extends State<CommentContainer> {
  String name="";
  String URL ="";
  int totalLike = 0;
  bool liked = false;

  Future? _navigationToCommentDetail() {
    return Get.to(
      CommentDetailScreen(
        comment: widget.comment,
        questionId: widget.questionId,
        totalLikes: totalLike,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }
  Future? _navigationToUserProfile() {
    print("tap");
    if(FirebaseAuth.instance.currentUser!.uid != widget.comment.commenterId) {
      return Get.to(
        UserProfileScreen(uid: widget.comment.commenterId),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }
  }

  Future getUserInfo() async {
    FirebaseFirestore.instance.collection("Users").doc(widget.comment.commenterId).get().then((snapshot){
      AppUser user = AppUser.fromJson(snapshot.data() as Map<String,dynamic>);
      setState(() {
        name = user.name;
        URL = user.profileURL;
      });
    }).catchError((e){

    });
  }

  Future getTotalLike() async {
    FirebaseFirestore.instance.collection("Surveys").doc(widget.questionId).collection("Comments").doc(widget.comment.commentId).collection("Likes").get().then(
            (value){
              setState(() {
                totalLike = value.size;
              });
            });
  }

  Future getUserLikedOrNot()async {
    var usr = FirebaseFirestore.instance.collection("Surveys").doc(widget.questionId).collection("Comments").doc(widget.comment.commentId).collection("Likes").doc(FirebaseAuth.instance.currentUser!.uid).get().then((value){
      if(value.exists){
        setState(() {
          liked=true;
        });
      }else{
        setState(() {
          liked = false;
        });
      }
    }).catchError((e){
      print(e);
    });
  }

  @override
  initState(){
    getTotalLike();
    getUserLikedOrNot();
    getUserInfo();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ListTile(
      trailing: widget.comment.commenterId == FirebaseAuth.instance.currentUser!.uid ? IconButton(icon: Icon(Icons.apps_rounded),onPressed: (){
        showModalBottomSheet(
            context: context,
            builder: (context) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: new Icon(Icons.delete_forever,color: Colors.grey),
                    title: new Text('Delete'),
                    onTap: () async {
                      //close the modal
                      // await Get.back();
                      await showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            backgroundColor: theme.primaryColor,
                            title: new Text("Delete Your Comment?"),
                            content: new Text("Are you sure you want to delete ?"),
                            actions: <Widget>[
                              new TextButton(
                                child: new Text("Cancel"),
                                onPressed: () async {
                                  Navigator.of(context).pop();
                                },
                              ),
                              new TextButton(
                                child: new Text("OK"),
                                onPressed: () async {
                                  // await deleteOwnReplyComment(widget.questionId,widget.comment.c,widget.reply.replyId);
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                  ListTile(

                  ),
                ],
              );
            });

      },):null,
      leading:GestureDetector(
        onTap: _navigationToUserProfile,
          child: Container(
            // padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              // color: Colors.blue.withOpacity(0.4)
            ),
            child: CachedNetworkImage(
                imageUrl: URL,
                imageBuilder: (context, imageProvider) => CircleAvatar(
                      backgroundImage: imageProvider,
                    ),
                placeholder: (context, url) => CircleAvatar(),
                errorWidget: (context, url, error) => CircleAvatar(
                    backgroundImage: AssetImage('assets/user.png')))),
      ),
      title: RichText(
        text: TextSpan(
          children:  <TextSpan>[
            TextSpan(text: '${name} ', style: theme.textTheme.caption),
            TextSpan(text: getDate(widget.comment.commentTime.toDate()),style: theme.textTheme.caption),
          ],
        ),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ///Comments
          Text(widget.comment.commentText!=null? widget.comment.commentText: "",style: theme.textTheme.bodyText1,),
          ///Like dislike Button
          SizedBox(height: 10,),
          Row(
            children: [
            GestureDetector( child: Icon(Icons.thumb_up,size: 18,color: liked? Colors.blue.withOpacity(0.5): Colors.grey.withOpacity(0.3)), onTap: ()async {
              if(liked == false) {
                await likeComment(FirebaseAuth.instance.currentUser!.uid,
                    widget.comment.commentId, widget.questionId);
              }
              else{
                await unLikeComment(FirebaseAuth.instance.currentUser!.uid,
                    widget.comment.commentId, widget.questionId);
              }
              await getUserLikedOrNot();
              await getTotalLike();

            },),
            Text(" ${totalLike.toString()} ",style: theme.textTheme.bodyText1,),
              Text("like",style: theme.textTheme.caption,)
            ],),
          SizedBox(height: 10,),
          ///reply button
          InkWell(
            onTap: _navigationToCommentDetail,
            child: Text("See Replies",style: theme.textTheme.subtitle1,),
          ),
          SizedBox(height: 10,),
          Divider(color: dividerColor,)
        ],
      ),
    );
  }
}
