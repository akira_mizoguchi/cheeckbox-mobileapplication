part of 'widgets.dart';

void _showDialog(BuildContext context, String questionId) {
  final commentTextController = TextEditingController();
  final theme = Theme.of(context);
  showModalBottomSheet(
      context: context,
      // barrierColor: Colors.black.withAlpha(1),
      builder: (builder) {
        return Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                  child: Container(
                decoration: new BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(20.0),
                        topRight: const Radius.circular(20.0))),
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: ListTile(
                  trailing: IconButton(
                    icon: Icon(
                      Icons.send,
                      color: Colors.grey,
                    ),
                    onPressed: () {
                      comment(
                          questionId,
                          FirebaseAuth.instance.currentUser!.uid,
                          commentTextController.text);
                      Get.back();
                    },
                  ),
                  title: TextField(
                    autofocus: true,
                    controller: commentTextController,
                    textInputAction: TextInputAction.newline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: Colors.grey),
                        hintText: "Write comment..."),
                  ),
                ),
              ))
            ]);
      });
}
