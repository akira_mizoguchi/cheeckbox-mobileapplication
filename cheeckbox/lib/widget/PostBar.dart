part of 'widgets.dart';

class PostBar extends StatefulWidget {
  final Question question;
  final int index;

  const PostBar({Key? key, required this.question, required this.index})
      : super(key: key);

  @override
  _PostBarState createState() => _PostBarState();
}

class _PostBarState extends State<PostBar> {
  String name = "";

  Future getUserInfo() async {
    FirebaseFirestore.instance
        .collection("Users")
        .doc(widget.question.creatorId)
        .get()
        .then((snapshot) {
      AppUser user = AppUser.fromJson(snapshot.data() as Map<String, dynamic>);
      setState(() {
        name = user.name;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  initState() {
    getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Future? _navigationToAnsweringScreen() {
      return Get.to(
        AnsweringScreen(
          questionId: widget.question.questionId,
        ),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: 500),
      );
    }

    return ListTile(
      onTap: _navigationToAnsweringScreen,
      leading: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.pink.withOpacity(0.4),
          ),
          child: Text(
            "${widget.index + 1}",
            style: theme.textTheme.headline5!.copyWith(color: Colors.white),
          )),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.red, borderRadius: BorderRadius.circular(30)),
            padding: EdgeInsets.only(left: 5, right: 10),
            child: Text(
              "Top Trend",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(text: 'Creator - ', style: theme.textTheme.caption),
                TextSpan(text: name, style: theme.textTheme.caption),
              ],
            ),
          ),
          Text(
            widget.question.questionText,
            style: theme.textTheme.headline5,
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              decoration: BoxDecoration(
                  color: Colors.orangeAccent,
                  borderRadius: BorderRadius.circular(30)),
              padding: EdgeInsets.only(left: 5, right: 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.check,
                    color: Colors.white,
                    size: 20,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(widget.question.total.toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.bold)),
                  Text(
                    " Answered",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  ),

                  // Text("pinned",style: theme.textTheme.caption,),
                  // IconButton(icon: Icon(Icons.playlist_add_outlined,color: Colors.grey,size: 30,),)
                ],
              )),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
