part of 'widgets.dart';

class ReplyCommentContainer extends StatefulWidget {
  final Reply reply;
  final String commentId;
  final String questionId;

  const ReplyCommentContainer(
      {Key? key,
      required this.reply,
      required this.questionId,
      required this.commentId})
      : super(key: key);

  @override
  _ReplyCommentContainerState createState() => _ReplyCommentContainerState();
}

class _ReplyCommentContainerState extends State<ReplyCommentContainer> {
  String profileURL = "";
  String name = "";
  String URL = "";
  int totalLike = 0;
  bool liked = false;

  Future? _navigationToUserProfile() {
    return Get.to(
      UserProfileScreen(
        uid: widget.reply.replyerId,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: milliseconds),
    );
  }

  Future getUserInfo() async {
    await FirebaseFirestore.instance
        .collection("users")
        .doc(widget.reply.replyerId)
        .get()
        .then((snapshot) {
      AppUser user = AppUser.fromJson(snapshot.data() as Map<String, dynamic>);
      setState(() {
        name = user.name;
        profileURL = user.profileURL;
      });
    }).catchError((e) {});
  }

  Future getTotalLike() async {
    FirebaseFirestore.instance
        .collection("Surveys")
        .doc(widget.questionId)
        .collection("Comments")
        .doc(widget.commentId)
        .collection("Replies")
        .doc(widget.reply.replyId)
        .collection("Likes")
        .get()
        .then((value) {
      setState(() {
        totalLike = value.size;
      });
    });
  }

  Future getUserLikedOrNot() async {
    var usr = FirebaseFirestore.instance
        .collection("Surveys")
        .doc(widget.questionId)
        .collection("Comments")
        .doc(widget.commentId)
        .collection("Replies")
        .doc(widget.reply.replyId)
        .collection("Likes")
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .get()
        .then((value) {
      if (value.exists) {
        setState(() {
          liked = true;
        });
      } else {
        setState(() {
          liked = false;
        });
      }
    }).catchError((e) {
      print(e);
    });
  }

  @override
  initState() {
    getTotalLike();
    getUserLikedOrNot();
    getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ListTile(
      trailing: widget.reply.replyerId == FirebaseAuth.instance.currentUser!.uid
          ? IconButton(
              icon: Icon(Icons.apps_rounded),
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: new Icon(Icons.delete_forever,
                                color: Colors.grey),
                            title: new Text('Delete'),
                            onTap: () async {
                              //close the modal
                              // await Get.back();
                              await showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    backgroundColor: theme.primaryColor,
                                    title: new Text("Alert!!"),
                                    content: new Text("Is it okay to delete ?"),
                                    actions: <Widget>[
                                      new FlatButton(
                                        child: new Text("OK"),
                                        onPressed: () async {
                                          await deleteOwnReplyComment(
                                              widget.questionId,
                                              widget.commentId,
                                              widget.reply.replyId);
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                          ListTile(),
                        ],
                      );
                    });
              },
            )
          : null,
      leading: GestureDetector(
        onTap: _navigationToUserProfile,
        child: Container(
            // padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              // color: Colors.blue.withOpacity(0.4)
            ),
            child: CachedNetworkImage(
                imageUrl: URL,
                imageBuilder: (context, imageProvider) => CircleAvatar(
                      backgroundImage: imageProvider,
                    ),
                placeholder: (context, url) => CircleAvatar(),
                errorWidget: (context, url, error) => CircleAvatar(
                    backgroundImage: AssetImage('assets/user.png')))),
      ),
      title: RichText(
        text: TextSpan(
          children: <TextSpan>[
            TextSpan(text: '${name} ', style: theme.textTheme.caption),
            TextSpan(
                text: getDate(widget.reply.replyTime.toDate()),
                style: theme.textTheme.caption),
          ],
        ),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ///Comments
          Text(
            widget.reply.replyText != null ? widget.reply.replyText : "",
            style: theme.textTheme.bodyText1,
          ),

          ///Like dislike Button
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              GestureDetector(
                child: Icon(
                  Icons.thumb_up,
                  size: 18,
                  color: liked ? Colors.blue.withOpacity(0.5) : Colors.grey,
                ),
                onTap: () async {
                  if (liked == false) {
                    await likeReply(
                        FirebaseAuth.instance.currentUser!.uid,
                        widget.commentId,
                        widget.reply.replyId,
                        widget.questionId);
                  } else {
                    await unLikeReply(
                        FirebaseAuth.instance.currentUser!.uid,
                        widget.reply.replyId,
                        widget.commentId,
                        widget.questionId);
                  }
                  await getUserLikedOrNot();
                  await getTotalLike();
                },
              ),
              Text(
                " ${totalLike.toString()} ",
                style: theme.textTheme.bodyText1,
              ),
              Text(
                "like",
                style: theme.textTheme.caption,
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),

          ///reply button
          SizedBox(
            height: 10,
          ),
          Divider(
            color: dividerColor,
          )
        ],
      ),
    );
  }
}
