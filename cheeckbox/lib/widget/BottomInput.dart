part of 'widgets.dart';

class BottomInput extends StatefulWidget {
  const BottomInput({Key? key}) : super(key: key);

  @override
  _BottomInputState createState() => _BottomInputState();
}

class _BottomInputState extends State<BottomInput> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: EdgeInsets.only(left: 10, bottom: 20, top: 10),
      height: 70,
      width: double.infinity,
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {},
            child: Container(
              height: 30,
              width: 30,
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 20,
              ),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Write message...", border: InputBorder.none),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          FloatingActionButton(
            backgroundColor: theme.primaryColor,
            onPressed: () {},
            child: Icon(
              Icons.send,
              size: 18,
            ),
            elevation: 0,
          ),
        ],
      ),
    );
  }
}
