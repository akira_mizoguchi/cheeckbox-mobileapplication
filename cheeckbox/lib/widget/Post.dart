part of 'widgets.dart';

class Post extends StatefulWidget {
  final Question question;

  Post({
    Key? key,
    required this.question,
  }) : super(key: key);

  @override
  _PostState createState() => _PostState();
}

class _PostState extends State<Post> {
  int votedNum = 0;

  Future? _navigationToAnsweringScreen() {
    return Get.to(
      AnsweringScreen(
        questionId: widget.question.questionId,
      ),
      curve: Curves.fastOutSlowIn,
      transition: Transition.fadeIn,
      duration: Duration(milliseconds: 500),
    );
  }

  Future _navigationToUserProfile() async {
    if (widget.question.creatorId == FirebaseAuth.instance.currentUser!.uid) {
    } else {
      return Get.to(
        UserProfileScreen(
          uid: widget.question.creatorId,
        ),
        curve: Curves.fastOutSlowIn,
        transition: Transition.fadeIn,
        duration: Duration(milliseconds: milliseconds),
      );
    }
  }

  void getVotedNum() {
    FirebaseFirestore.instance
        .collection("Surveys")
        .doc(widget.question.questionId)
        .collection("Answers")
        .get()
        .then((snapshot) {
      print(snapshot.size);
      setState(() {
        votedNum = snapshot.size;
      });
    }).catchError((e) {
      votedNum = 0;
    });
  }

  @override
  initState() {
    getVotedNum();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    double width = displayWidth(context);
    double height = displayHeight(context);
    return InkWell(
        onTap: _navigationToAnsweringScreen,
        child: Padding(
            padding: EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FutureBuilder(
                  future: getUserbyUid(widget.question.creatorId),
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      AppUser user = AppUser.fromJson(snapshot.data.data());
                      return GestureDetector(
                          onTap: _navigationToUserProfile,
                          child: ListTile(
                            leading: Container(
                                // padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.blue.withOpacity(0.4)),
                                child: CachedNetworkImage(
                                  imageUrl: user.profileURL,
                                  imageBuilder: (context, imageProvider) =>
                                      CircleAvatar(
                                    backgroundImage: imageProvider,
                                  ),
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(),
                                  errorWidget: (context, url, error) =>
                                      CircleAvatar(
                                          backgroundImage:
                                              AssetImage('assets/user.png')),
                                )),
                            title: RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: user.name,
                                    style: theme.textTheme.bodyText1,
                                  ),
                                  TextSpan(
                                      text: "-@${user.username}",
                                      style: theme.textTheme.caption),
                                ],
                              ),
                            ),
                            subtitle: Text(
                              getDate(widget.question.postTime.toDate()),
                              style: theme.textTheme.caption,
                            ),
                            // trailing: IconButton(
                            //   icon: Icon(
                            //     Icons.menu,
                            //     color: Colors.grey,
                            //   ),
                            //   onPressed: () {},
                            // ),
                          ));
                    } else {
                      return Container();
                    }
                  },
                ),
                ContentContainer(
                  question: widget.question,
                ),
                Container(
                    margin: EdgeInsets.only(left: 5),
                    decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.circular(30)),
                    padding: EdgeInsets.only(left: 5, right: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(votedNum.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                        Text(
                          " Answered",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),

                        // Text("pinned",style: theme.textTheme.caption,),
                        // IconButton(icon: Icon(Icons.playlist_add_outlined,color: Colors.grey,size: 30,),)
                      ],
                    )),
                SizedBox(
                  height: 15,
                ),
                Divider()
              ],
            )));
  }
}
