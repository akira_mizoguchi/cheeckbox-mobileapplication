part of 'widgets.dart';

class FutureBarChart extends StatelessWidget {
  final String questionId;

  const FutureBarChart({required this.questionId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getSurveyAnswerList(questionId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          List<int> eachTotal = snapshot.data;
          print(eachTotal);
          return MyFadeTransition(
              child: BarResultChart(
            eachTotal: [22,333,33],
          ));
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else {
          return Container();
        }
      },
    );
  }
}
