part of 'widgets.dart';

class FutureTrendPostBar extends StatefulWidget {
  const FutureTrendPostBar({Key? key}) : super(key: key);

  @override
  _FutureTrendPostBarState createState() => _FutureTrendPostBarState();
}

class _FutureTrendPostBarState extends State<FutureTrendPostBar> {
  Future<void> _reload() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Widget noContext() {
      return Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          "No Content",
          style: theme.textTheme.headline3,
        ),
        Text(
          "Find Survey You Want",
          style: theme.textTheme.bodyText2,
        )
      ]));
    }

    return RefreshIndicator(
        onRefresh: _reload,
        child: FutureBuilder(
          future: getMostVotedQuestion(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              //when the data has no data (size == 0) return nonContext()
              if (snapshot.data!.size == 0) {
                return noContext();
              } else {
                return ListView.separated(
                    itemCount: snapshot.data!.docs.length,
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemBuilder: (context, index) {
                      if (index < 30) {
                        Question question = Question.fromJsonFireStore(
                            snapshot.data!.docs[index].data()
                                as Map<String, dynamic>);
                        return MyFadeTransition(
                            child: PostBar(
                          question: question,
                          index: index,
                        ));
                      } else {
                        return Container();
                      }
                    });
              }
            } else {
              //this is return when the snapshot will return when there is no has data
              return noContext();
            }
          },
        ));
  }
}
