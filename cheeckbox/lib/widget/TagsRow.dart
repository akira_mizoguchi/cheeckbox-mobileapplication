part of 'widgets.dart';

class TagsRow extends StatelessWidget {
  final List<dynamic> tags;

  const TagsRow({Key? key, required this.tags}) : super(key: key);

  String allTags() {
    String line = "";
    tags.forEach((tag) {
      line += "#" + tag + " ";
    });
    return line;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30), color: Colors.blue),
      child: Text(
        allTags(),
        style: TextStyle(
            color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
      ),
    );
  }
}
