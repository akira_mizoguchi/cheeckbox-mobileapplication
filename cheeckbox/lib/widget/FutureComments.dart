part of 'widgets.dart';

///Sliver
class FutureSliverComments extends StatelessWidget {
  final String questionId;
  final Future<QuerySnapshot> future;

  const FutureSliverComments(
      {required this.questionId, Key? key, required this.future})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          return SliverList(
              delegate: SliverChildListDelegate(snapshot.data!.docs.map(
            (DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data() as Map<String, dynamic>;
              final Comment comment = Comment.fromJson(data);
              print(comment.commentText);
              return MyFadeTransition(
                  child: CommentContainer(
                comment: comment,
                questionId: questionId,
              ));
            },
          ).toList()));
        } else {
          // We can show the loading view until the data comes back.
          return SliverToBoxAdapter(child: Container());
        }
      },
    );
  }
}

class FutureReplyComments extends StatefulWidget {
  final String questionId;
  final String commentId;

  const FutureReplyComments(
      {Key? key, required this.questionId, required this.commentId})
      : super(key: key);

  @override
  _FutureReplyCommentsState createState() => _FutureReplyCommentsState();
}

class _FutureReplyCommentsState extends State<FutureReplyComments> {
  Future<void> _loadData() async {
    setState(() {
      getCommentDetailReplys(widget.commentId, widget.questionId);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    // future = getCommentDetailReply(widget.commentId,widget.questionId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
        onRefresh: _loadData,
        child: FutureBuilder(
          future: getCommentDetailReplys(widget.commentId, widget.questionId)
              as Future<QuerySnapshot>,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              return ListView(
                  children: (snapshot.data!.docs.map(
                (DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data() as Map<String, dynamic>;
                  final Reply reply = Reply.fromJson(data);
                  if (reply != null) {
                    return MyFadeTransition(
                        child: ReplyCommentContainer(
                      reply: reply,
                      questionId: widget.questionId,
                      commentId: widget.commentId,
                    ));
                  } else {
                    return Container();
                  }
                },
              ).toList()));
            } else {
              // We can show the loading view until the data comes back.
              return Center(child: CircularProgressIndicator());
            }
          },
        ));
  }
}
