part of 'widgets.dart';

class CommentInput extends StatefulWidget {
  final String questionId;

  const CommentInput({Key? key, required this.questionId}) : super(key: key);

  @override
  _CommentInputState createState() => _CommentInputState();
}

class _CommentInputState extends State<CommentInput> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ListTile(
        tileColor: theme.primaryColor,
        leading: Icon(Icons.comment_sharp),
        title: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(32)),
            onTap: () {
              _showDialog(context, widget.questionId);
            },
            child: InkWell(
              borderRadius: BorderRadius.all(Radius.circular(32)),
              onTap: () {
                _showDialog(context, widget.questionId);
              },
              child: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.grey.withOpacity(0.2)),
                child: Text(
                  "Write Comment....",
                  textAlign: TextAlign.start,
                  style: TextStyle(color: Colors.grey, fontSize: 16),
                ),
              ),
            )));
  }
}
