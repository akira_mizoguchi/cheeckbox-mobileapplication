part of 'widgets.dart';

class Picker extends StatelessWidget {
  final String pickertitle;
  String gender = "Male";
  final List<String> genderList = [
    "Male",
    "Female",
  ];
  final List<Text> genTextList = [Text("Male"), Text("Female")];

  Picker({Key? key, required this.pickertitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    var size = MediaQuery.of(context).size;
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            padding: const EdgeInsets.only(top: 10),
            height: size.height * 2 / 5,
            child: Column(
              children: [
                Flexible(
                    flex: 1,
                    child: Text(
                      pickertitle,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )),
                Flexible(
                    flex: 4,
                    child: CupertinoPicker(
                        backgroundColor: Colors.white,
                        itemExtent: 30,
                        scrollController:
                            FixedExtentScrollController(initialItem: 0),
                        onSelectedItemChanged: (int selected) {
                          gender = genderList[selected];
                        },
                        children: genTextList)),
                Flexible(
                    flex: 1,
                    child: Container(
                        width: displayWidth(context) * 0.7,
                        child: FlatButton(
                          color: theme.primaryColor,
                          shape: StadiumBorder(),
                          child: Text("Close"),
                          onPressed: () {
                            Navigator.pop(context, gender);
                          },
                        )))
              ],
            )));
  }
}

class DatePicker extends StatelessWidget {
  DateTime selectedDate = DateTime(2000);

  late Function(int) onChange;

  // var today= new DateTime(now.year, now.month, now.day);
  // final _BodyState parent;
  final String pickertitle;

  DatePicker({Key? key, required this.pickertitle, required this.onChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final theme = Theme.of(context);
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            padding: const EdgeInsets.only(top: 10),
            height: size.height * 2 / 5,
            child: Column(
              children: [
                Flexible(
                    flex: 1,
                    child: Text(
                      pickertitle,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )),
                Flexible(
                    flex: 4,
                    child: CupertinoDatePicker(
                      initialDateTime: selectedDate,
                      onDateTimeChanged: (DateTime newDate) {
                        selectedDate = newDate;
                        // print(selectedDate);
                      },
                      minimumYear: 1920,
                      maximumYear: 2050,
                      mode: CupertinoDatePickerMode.date,
                    )),
                Flexible(
                    flex: 1,
                    child: Container(
                        width: displayWidth(context) * 0.7,
                        child: FlatButton(
                          color: theme.primaryColor,
                          shape: StadiumBorder(),
                          child: Text("Close"),
                          onPressed: () {
                            // print(selectedDate);
                            Navigator.pop(context, selectedDate);
                          },
                        )))
              ],
            )));
  }
}
